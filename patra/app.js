var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var crypto = require('crypto');
var cors = require('cors');

var swig = require('swig');
var passport = require('passport');
var session  = require('express-session');
var LocalStrategy = require('passport-local').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/patra');

var index = require('./routes/index');
var dashboard = require('./routes/dashboard');
var users = require('./routes/users');
var jadwal = require('./routes/jadwal');
var tugas = require('./routes/tugas');
var kegiatan = require('./routes/kegiatan');
var matakuliah = require('./routes/matakuliah');
var timeline = require('./routes/timeline');
var informasi = require('./routes/informasi');
var arsip = require('./routes/arsip');
var aspirasi = require('./routes/aspirasi');
var kuisioner = require('./routes/kuisioner');
var kuisionerresult = require('./routes/kuisionerresult');
var settings = require('./routes/settings');
var api = require('./routes/api');
var apiuser = require('./routes/api_user');

// models
var User = require('./models/user');

mongoose.Promise = require('bluebird');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.engine('html', swig.renderFile);
app.set('view engine','ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(session({ secret: 'iniadalahrahasia' })); // session secret
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.use(cors());

passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user){
        if(err){
        	// console.log( err );
        	done(null, false); 
        }
        if(user){
        	done(null, user);	
        }
    });
});

passport.use( "local", new LocalStrategy( {passReqToCallback: true},
    function(req, username, password, done) {
        User.findOne({username: username, password: password, status: true}, function(err, user){
        	if(err){
        		req.flash('message', 'Username or password wrong');
        		done(null, false); 
        	}
        	if(user){
            if(user.level!='admin'){
              req.flash('message', 'User not authorized');
              done(null, false);
            }else
              done(null, user);
        	}else{
            req.flash('message', 'Username or password wrong');
        		done(null, false);
        	}
        });
    }
));

passport.use( "access", new LocalStrategy( {passReqToCallback: true},
    function(req, username, password, done) {
        User.findOne({username: username, password: password, status: true}, function(err, user){
          if(err){
            done(null, false); 
          }
          if(user){
            var token = crypto.randomBytes(64).toString('hex');
            user.token = token;
            user.save().then(function(usr){
              done(null, user);
            });
          }else{
            done(null, false);
          }
        });
    }
));

passport.use( "bearer", new BearerStrategy(
  function(token, done) {
    // console.log('token',token);
    User.findOne({ token: token }, function (err, user) {
      if (err) { return done(err); }
      // console.log('a');
      if (!user) { return done(null, false); }
      // console.log('user',user);
      return done(null, user, { scope: 'all' });
    });
  }
));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/resources', express.static(path.join(__dirname, 'resources')));
app.use('/', express.static(path.join(__dirname, 'bower_components')));
app.use('/', express.static(path.join(__dirname, 'files')));

function haveLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        res.redirect('/dashboard');
    }
}
function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        console.log('-----------------------------');
        console.log(req.user);
        return next();
    }
    res.redirect('/');
}

app.use('/', index);
app.use('/dashboard', isLoggedIn, dashboard);
app.use('/data-pengguna', isLoggedIn, users);
app.use('/jadwal-kuliah', isLoggedIn, jadwal);
app.use('/tugas-mingguan', isLoggedIn, tugas);
app.use('/kegiatan-patra', isLoggedIn, kegiatan);
app.use('/matakuliah', isLoggedIn, matakuliah);
app.use('/karya', isLoggedIn, timeline);
app.use('/informasi-bulanan', isLoggedIn, informasi);
app.use('/arsip-data', isLoggedIn, arsip);
app.use('/aspirasi', isLoggedIn, aspirasi);
app.use('/kuisioner', isLoggedIn, kuisioner);
app.use('/kuisioner-result', isLoggedIn, kuisionerresult);
app.use('/settings', isLoggedIn, settings);
app.post('/login', passport.authenticate('local', { failureRedirect: '/'}), 
    function(req, res) {
        res.redirect('/dashboard');
    }
);
// app.use('/api', passport.authenticate('bearer', { session: false }), api);
app.use('/api', api);
app.use('/apiuser', passport.authenticate('bearer', { session: false }), apiuser);
app.post('/access', function(req, res, next) {
  passport.authenticate('access', function(err, user, info) {
    if (err){
      res.send({status:false})
      return;
    }
    if (!user){
      res.send({status:false});
      return;
    }
    req.logIn(user, function(err) {
      if (err){
        res.send({status:false});
        return;
      }
      res.send({status:true, user:user});
    });
  })(req, res, next);
});
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

// app.use(function(req, res, next) {
//   	var err = new Error('Not Found');
//   	err.status = 404;
//   	next(err);
// });

// error handler
app.use(function(err, req, res, next) {
	console.log('error happened ', err);
  	// set locals, only providing error in development
  	res.locals.message = err.message;
  	res.locals.error = req.app.get('env') === 'development' ? err : {};
  	// render the error page
  	res.status(err.status || 500);
  	res.render('error');
});

app.listen( 8000 );

module.exports = app;
