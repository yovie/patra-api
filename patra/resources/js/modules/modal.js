var Modal = {

	modal_html: '<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">'
  				+	'<div class="modal-dialog">'
    			+	'<div class="modal-content">'
      			+	'<div class="modal-header">'
        		+	'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
        		+	'<h4 class="modal-title"><%= title %></h4>'
      			+	'</div>'
      			+	'<div class="modal-body">'
        		+	'<p><%= message %></p>'
      			+	'</div>'
      			+	'<div class="modal-footer">'
        		+	'<button type="button" class="btn btn-default" data-dismiss="modal"> <span class="fa fa-remove"></span> <%= no_action_label %></button>'
        		+	'<button type="button" class="btn btn-<%= kind %>" data-info=\'<%= info_json %>\' onclick="<%= action %>"> <span class="fa fa-forward"></span> <%= action_label %></button>'
      			+	'</div>'
    			+	'</div>'
  				+	'</div>'
				+	'</div>',

	detail_html: '<div class="modal fade" id="detailModal" tabindex="-1" role="dialog">'
  				+	'<div class="modal-dialog" style="width:60%">'
    			+	'<div class="modal-content">'
      			+	'<div class="modal-header">'
        		+	'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
        		+	'<h4 class="modal-title"><%= title %></h4>'
      			+	'</div>'
      			+	'<div class="modal-body">'
        		+	'<p><%= message %></p>'
      			+	'</div>'
      			+	'<div class="modal-footer">'
        		+	'<button type="button" class="btn btn-default" data-dismiss="modal"> <span class="fa fa-remove"></span> <%= no_action_label %></button>'
      			+	'</div>'
    			+	'</div>'
  				+	'</div>'
				+	'</div>',

	confirm: function(title, message, no_action_label, action_label, info_json, action, kind){
		this.clear();
		var tpl = _.template(this.modal_html);
		var mdl = tpl({
			title: title,
			message: message,
			no_action_label: no_action_label,
			action_label: action_label,
			info_json: JSON.stringify(info_json),
			action: action,
			kind: kind
		});
		$(mdl).modal('show'); 
	},

	detail: function(title, message, no_action_label){
		this.clear();
		var tpl = _.template(this.detail_html);
		var mdl = tpl({
			title: title,
			message: message,
			no_action_label: no_action_label
		});
		$(mdl).modal('show'); 
	},

	hide: function(){
		$('#confirmModal').modal('hide');
	},

	clear: function(){
		$('#confirmModal').remove();
	}

}