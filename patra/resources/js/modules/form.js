$(function(){
	

	$('#form').on('shown.bs.modal', function (e) {
	    var b = $(e.relatedTarget);
	    var detail = b.data('detail');

	    if(typeof(detail)=='string'){
			var row = parseInt(b.data('row'));
			if(window.rowDetail[row]!=undefined)
				detail = window.rowDetail[row];
			if(typeof(detail)!='object')
				return;
	    }

	    if(detail!=null){
		    for(field in detail){
		    	if(field[0]!='_'){
		    		var ci = $('#form').find('input[name=' + field + ']');
		    		var cs = $('#form').find('select[name=' + field + ']');
		    		var ct = $('#form').find('textarea[name=' + field + ']');
		    		var sel = undefined;
		    		if(ci.length>0)
		    			sel = ci;
		    		if(cs.length>0){
		    			sel = cs;
		    		}
		    		if(ct.length>0)
		    			sel = ct;
		    		if(sel!=undefined && sel.attr('type')!='file'){
		    			if(detail[field]!=null && detail[field]!='null'){
		    				// console.log(detail[field]);
		    				sel.val(detail[field]);
		    				if(sel.prop('tagName')=='SELECT')
		    					sel.trigger("chosen:updated");
			    			if(typeof ckActive!=="undefined"){
			    				if(ckActive==true && ckField==field){
			    					CKEDITOR.instances[field].setData(detail[field]);
			    				}
			    			}	
		    			}else
			    			sel.val('');
		    		}
		    	}
		    }
		    $('#form').find('input[name=id]').val(detail['_id']);
		    $('#form').find('form').attr('action', dtModule + '/update');
		}else{
			var ci = $('#form').find('input');
		    var cs = $('#form').find('select');
		    var ct = $('#form').find('textarea');
		    for(i=0;i<ci.length;i++)
		    	ci.eq(i).val('');
		    for(i=0;i<cs.length;i++){
		    	cs.eq(i).val('');
		    	cs.eq(i).trigger("chosen:updated");
		    }
		    for(i=0;i<ct.length;i++)
		    	ct.eq(i).val('');
		    if(ckActive==true){
				CKEDITOR.instances[ckField].setData('');
			}
		    $('#form').find('input[name=id]').val('');
			$('#form').find('form').attr('action', dtModule + '/save');
		}
		$('#form').find('select').chosen();
	} );

	/*for user*/
	$('#formAkun').on('shown.bs.modal', function (e) {
	    var b = $(e.relatedTarget);
	    var detail = b.data('detail');
	    if(detail!=null){
	    	$.get(dtModule + '/account/' + detail.userRef, function(res){
	    		if(res.status){
	    			$('#formAkun').find('input[name=username]').val(res.data.username);
	    			$('#formAkun').find('input[name=id]').val(res.data._id);
	    		}
	    	},'json');
	    }
	});

	$('#datetimepicker1').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	$('#datetimepicker2').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	$('#datetimepicker3').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});

	$('#timepicker1').timepicker({
		showMeridian: false
	});
	$('#timepicker2').timepicker({
		showMeridian: false
	});
	$('#timepicker3').timepicker({
		showMeridian: false
	});

});