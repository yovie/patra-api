$(function(){

	$('.table-data').dataTable({
		"aoColumnDefs": [
        	{ 'bSortable': false, 'aTargets': dtDisableSort }
		],
		"aaSorting": []
	});

});

function  action_delete(th,num){
	var data = $(th).data('detail');
	data = (data!=null) ? data:{};
	if(typeof(data)=='string'){
		if(num!=undefined){
			data = window.rowDetail[num];
			Modal.confirm('Confirmation', 'Yakin akan dihapus ?', 'Batal', 'Hapus', data, '_do_post(this,'+num+')', 'danger');
		}
	}else
		Modal.confirm('Confirmation', 'Yakin akan dihapus ?', 'Batal', 'Hapus', data, '_do_post(this)', 'danger');
}

function  action_reset(th){
	var data = $(th).data('detail');
	data = (data!=null) ? data:{};
	Modal.confirm('Confirmation', 'Yakin akan mereset password menjadi "12345" ?', 'Batal', 'Reset', data, '_do_reset_password(this)', 'warning');	
}

function  action_disable(th){
	var data = $(th).data('detail');
	data = (data!=null) ? data:{};
	Modal.confirm('Confirmation', 'Yakin akan merubah status ?', 'Batal', 'Ubah', data, '_do_update_status(this)', 'warning');	
}

function _do_post(th,num){
	Modal.hide();
	var data = $(th).data('info');
	if(typeof(data)=='string'){
		if(num!=undefined){
			data = window.rowDetail[num];
		}
	}
	// console.log(typeof(data),data._id);
	$.post(dtModule + '/delete', {id:data._id}, function(res){
		location.reload();
	});
}

function _do_update_status(th){
	Modal.hide();
	var data = $(th).data('info');
	$.post(dtModule + '/disable', {id:data._id, status: !data.status}, function(res){
		location.reload();
	});
}

function _do_reset_password(th){
	Modal.hide();
	var data = $(th).data('info');
	$.post(dtModule + '/account/reset', {id:data.userRef}, function(res){
		location.reload();
	});
}

function activate_datatable(sel, dis_sort){
	sel.dataTable({
		"aoColumnDefs": [
        	{ 'bSortable': false, 'aTargets': dis_sort }
		],
		"aaSorting": []
	});
}

function _show_detail(th){
	var ctn = $(th).find('#rowContent');
	Modal.detail('Detail', ctn.html(), 'Tutup');
}