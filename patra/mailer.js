'use strict';

const nodemailer = require('nodemailer');
const async = require('async');
const moment = require('moment');

var Kegiatan = require('./models/kegiatan');
var Profile = require('./models/profile');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/patra');

let transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'patraitbservices',
        pass:  'poiuy123'
    },
    debug: true
}, {
    from: 'PATRA Services <patraitbservices@gmail.com>',
    headers: {
    }
});

var daftar_kegiatan = new Array;
var daftar_mahasiswa = new Array;
var user_to_get = 0;
var nw = new Date();

console.log('Start');

function resetSentStatus(){
  console.log('reset sent mail status');
  Profile.find({position:'anggota', status: true}).exec().then(function(profiles){
    async.forEach(profiles, function(profile, cbb){
      profile.sent_mail = false;
      profile.save().then(function(ori){
      }).catch(function(err){
        console.log(err);
      });
      cbb();
    }, function(err){
      if(err)
        console.log(err);
    });
  }).catch(function(err){
    console.log(err);
  });
}

async.waterfall([
  
  function(cb){
    console.log('get kegiatan');
    Kegiatan.find({pelaksanaan: {$gt:nw}}).exec().then((data)=>{
      if(data){
        daftar_kegiatan = data;
      }
      if(daftar_kegiatan.length>0)
        user_to_get = parseInt(100/daftar_kegiatan.length);
      // console.log('total kegiatan ' + daftar_kegiatan.length);
      console.log('user to get ' + user_to_get);
      // console.log(data);
      cb();
    });
  },

  function(cb){
    console.log('get profile');
    Profile.find({position:'anggota', status: true, sent_mail: false}).limit(user_to_get).exec().then((data)=>{
      if(data){
        daftar_mahasiswa = data;
        if(daftar_mahasiswa.length<=70)
          resetSentStatus();
      }else{
        resetSentStatus();
      }
      cb();
    });
  },

  function(cb){
    console.log('send mail');
    async.forEachSeries(daftar_kegiatan, function(item, cbb){
      var a = new Date(item.pelaksanaan);
      var b = new Date(item.sampai);
      var fdata = {
        _id: item._id,
        nama: item.nama,
        deskripsi: item.deskripsi,
        pelaksanaan: item.pelaksanaan==null ? '':moment(a).format('DD-MM-YYYY'),
        sampai: item.sampai==null ? '':moment(b).format('DD-MM-YYYY'),
        kontakRef: item.kontakRef,
        gambar: item.gambar,
        kontak: '',
        expired: false
      };
      if(item.pelaksanaan!=null){
        if(nw>=item.pelaksanaan)
          fdata.expired = true;
      }

      console.log(fdata.nama);
      if(fdata.expired){
        cbb();
      }else{
        async.forEachSeries(daftar_mahasiswa, function(mitem, mcbb){
          var send = true;
          if(Array.isArray(item.attendance)){
            for(let i=0; i<item.attendance.length; i++){
              if(item.attendance[i].member.equals(mitem._id)){
                send = false;
              }
            }
          }
          if(send){
            console.log('sent ---------------- ' + mitem.name +', '+ mitem.email);
            let message = {

              to: mitem.name + ' <' +mitem.email+ '>',
              // to: 'Yopi ilkom <yovieilkom@gmail.com>',
              // to: 'Yopi <yopi.puhun.jaya@gmail.com>',

              subject: 'Konfirmasi Kehadiran, ' + fdata.nama,

              html: '<h3>' +fdata.nama+ '</h3>'
                + '<p>Anda menerima email ini karena belum melakukan konfirmasi kehadiran. '
                + 'Konfirmasi kehadiran dapat dilakukan melalui aplikasi mobile PATRA.</p>'
                + '<p>Kegiatan akan dilaksanakan pada tanggal ' +fdata.pelaksanaan
                + ( (fdata.sampai!=null) ? ' sampai ' + fdata.sampai : '') + '.</p>'
                + '<br/><p>Terima kasih</p><br/>',
                // + '<p><strong>PATRA Services</strong></p>'

              // watchHtml: '<b>Hello</b> to myself',

              attachments: []
            };

            transporter.sendMail(message, (error, info) => {
              if (error) {
                console.log('Error occurred');
                console.log(error.message);
                return;
              }
              // console.log('Message sent successfully!');
              console.log('Server responded with "%s"', info.response);
              mcbb();
              // mcbb(new Error('I want to exit here'));
            });

            // mcbb(new Error('I want to exit here'));
          }else
            mcbb();
        }, function(err){
          cbb();
          // cbb(new Error('I want to exit here'));
        });
      }

    }, function(err){
      if(err)
        console.log(err);
      cb();
    });
  },

  function(cb){
    console.log('update send mail status');
    async.forEach(daftar_mahasiswa, function(item, cbb){
      item.sent_mail = true;
      item.save().then(function(ori){
        console.log('all sent mail status reseted');
      }).catch(function(err){
        console.log(err);
      });
      cbb();
    }, function(err){
      cb();
    });
  }

], function(err){
  if(err)
    console.log(err);
  transporter.close();
  console.log('done')
});