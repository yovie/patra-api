var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var kuisionerUserSchema = new Schema({
	kuisionerDetailRef: mongoose.Schema.Types.ObjectId,
	kontakRef: mongoose.Schema.Types.ObjectId,
	tipe: {
 		type: String,
 		enum: ['choice', 'text'],
 		default: 'choice'
 	},
 	choiceValue: String,
 	textValue: String,
  	created_at: Date,
  	updated_at: Date
});


var KuisionerUser = mongoose.model('KuisionerUser', kuisionerUserSchema);

module.exports = KuisionerUser;
