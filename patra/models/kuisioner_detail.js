var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var kuisionerDetailSchema = new Schema({
	kuisionerRef: mongoose.Schema.Types.ObjectId,
 	pertanyaan: String,
 	tipe: {
 		type: String,
 		enum: ['choice', 'text'],
 		default: 'choice'
 	},
 	choice: [{
 		label: String,
 		value: String
 	}],
  	created_at: Date,
  	updated_at: Date
});


var KuisionerDetail = mongoose.model('KuisionerDetail', kuisionerDetailSchema);

module.exports = KuisionerDetail;
