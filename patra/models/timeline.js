var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var timelineSchema = new Schema({
  timeline: String,
  detail: String,
  awal: Date,
  akhir: {type: Date, default: null},
  divisi: String,
  gambar: {type: String, default: null},
  created_at: Date,
  updated_at: Date
});


var Timeline = mongoose.model('Timeline', timelineSchema);

module.exports = Timeline;
