var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var aspirasiSchema = new Schema({  
	kontakRef: {
	  	type: mongoose.Schema.Types.ObjectId,
	  	default: null
	},
	tanggal: Date,
	judul: String,
	detail: String,
	deleted: {
		type: Boolean,
		default: false
	},
	created_at: Date,
  	updated_at: Date
});


var Aspirasi = mongoose.model('Aspirasi', aspirasiSchema);

module.exports = Aspirasi;
