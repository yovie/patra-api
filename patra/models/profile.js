var mongoose = require('mongoose');
// var moment = require('moment');
var Schema = mongoose.Schema;

var profileSchema = new Schema({
  ni: {type: String, default: null, unique: true},
  name: String,
  gender: {type: String, enum:['lk','pr'], default: 'lk'},
  date_birth: {type: Date, default: null},
  place_birth: {type: String, default: null},
  address: {type: String, default: null},
  address_bandung: {type: String, default: null},
  email: {type: String, default: null},
  phone: {type: String, default: null},
  phone2: {type: String, default: null},
  lineid: {type: String, default: null},
  position: {type: String, enum:['dosen','tu','anggota','alumni'], default: 'anggota'},
  level: {type: String, default: null},
  date_in: {type: Date, default: null},
  date_out: {type: Date, default: null},
  photo: {type: String, default: 'user.png'},
  userRef: {
  	type: mongoose.Schema.Types.ObjectId,
  	default: null
  },
  last_login: {type: Date, default: null},
  status: {type: Boolean, default: true},
  sent_mail: {type: Boolean, default: true},
  created_at: Date,
  updated_at: Date
});

// profileSchema.virtual('date_birth').set(function(nv){
//   this.date_birth = nv;
// });


var Profile = mongoose.model('Profile', profileSchema);

module.exports = Profile;
