var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var arsipSchema = new Schema({
  nama: String,
  keterangan: String,
  berkas: String,
  gambar: {type: String, default: null},
  created_at: Date,
  updated_at: Date
});


var Arsip = mongoose.model('Arsip', arsipSchema);

module.exports = Arsip;
