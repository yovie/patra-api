var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  token: { type: String, unique: true },
  email: {type: String, default: null},
  profileRef: mongoose.Schema.Types.ObjectId,
  status: {type: Boolean, default: true},
  level: {type:String, enum:['admin','operator','member'], default:'member'},
  created_at: Date,
  updated_at: Date
});

var User = mongoose.model('User', userSchema);

module.exports = User;