var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var jadwalSchema = new Schema({
  hari: {type: String, enum:['senin','selasa','rabu','kamis','jumat','sabtu','minggu'], default: 'senin'},
  jam_mulai: String,
  jam_selesai: String,
  jammulai: Number,
  matakuliahRef: {
  	type: mongoose.Schema.Types.ObjectId,
  	default: null
  },
  angkatan: Number,
  created_at: Date,
  updated_at: Date
});


var Jadwal = mongoose.model('Jadwal', jadwalSchema);

module.exports = Jadwal;
