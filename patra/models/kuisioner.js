var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var kuisionerSchema = new Schema({
 	judul: String,
 	deskripsi: String,
 	status: {type:String, enum:['close','open','lock'], default: 'close'},
 	gambar: {type: String, default: null},
  	created_at: Date,
  	updated_at: Date
});


var Kuisioner = mongoose.model('Kuisioner', kuisionerSchema);

module.exports = Kuisioner;
