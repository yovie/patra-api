var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tugasSchema = new Schema({
  mulai: Date,
  jenis: {type: String, enum:['individu','kelompok'], default: 'individu'},
  matakuliahRef: {
  	type: mongoose.Schema.Types.ObjectId,
  	default: null
  },
  judul: String,
  rincian: String,
  selesai: Date,
  created_at: Date,
  updated_at: Date
});


var Tugas = mongoose.model('Tugas', tugasSchema);

module.exports = Tugas;
