var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var kegiatanSchema = new Schema({
  nama: String,
  deskripsi: String,
  pelaksanaan: Date,
  sampai: {type: Date, default: null},
  kontakRef: {
  	type: mongoose.Schema.Types.ObjectId,
  	default: null
  },
  attendance: [
    {
      member: {type: mongoose.Schema.Types.ObjectId, default: null},
      status: {
        type: String,
        enum: ['yes', 'no', 'maybe'],
        default: 'yes'
      },
      confirm: {type: Date, default: null}
    }
  ],
  gambar: {type: String, default: null},
  created_at: Date,
  updated_at: Date
});


var Kegiatan = mongoose.model('Kegiatan', kegiatanSchema);

module.exports = Kegiatan;
