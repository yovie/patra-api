var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var informasiSchema = new Schema({
  judul: String,
  detail: String,
  bulan: Date,
  gambar: {type: String, default: null},
  created_at: Date,
  updated_at: Date
});


var Informasi = mongoose.model('Informasi', informasiSchema);

module.exports = Informasi;
