var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var matakuliahSchema = new Schema({
  kode: String,
  matakuliah: String,
  sks: Number,
  dosenRef: {
  	type: mongoose.Schema.Types.ObjectId,
  	default: null
  },
  gambar: {type: String, default: null},
  created_at: Date,
  updated_at: Date
});

/*matakuliahSchema.virtual('nama_dosen').set(function(nm){
  this.nama_dosen = nm;
}).get(function(){
  return this.nama_dosen;
});*/

var Matakuliah = mongoose.model('Matakuliah', matakuliahSchema);

module.exports = Matakuliah;
