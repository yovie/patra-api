var mongoose = require('mongoose');
// var moment = require('moment');
var Schema = mongoose.Schema;

var tmpprofileSchema = new Schema({
  ni: {type: String, default: null},
  name: String,
  date_birth: {type: Date, default: null},
  place_birth: {type: String, default: null},
  address: {type: String, default: null},
  address_bandung: {type: String, default: null},
  email: {type: String, default: null},
  phone1: {type: String, default: null},
  phone2: {type: String, default: null},
  lineid: {type: String, default: null},
  angkatan: String,
  created_at: Date,
  updated_at: Date
});

var Tmpprofile = mongoose.model('Tmpprofile', tmpprofileSchema);

module.exports = Tmpprofile;
