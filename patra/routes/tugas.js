var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var Entities = require('html-entities').AllHtmlEntities;

var Tugas = require('../models/tugas');
var Matakuliah = require('../models/matakuliah');
var Profile = require('../models/profile');

var params = {
	module: 'tugas-mingguan'
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

router.get('/', function(req, res, next) {
	params.data = new Array;
	params.matakuliah = new Array;
	entities = new Entities();
	async.parallel([
			function(cb){
				Matakuliah.find().exec().then(function(data){
					async.forEach(data, function(item, cbb){
						Profile.findOne({_id:item.dosenRef}).exec().then(function(dosen){
							params.matakuliah.push({
								_id: item._id,
								kode: item.kode,
								matakuliah: item.matakuliah,
								sks: item.sks,
								dosenRef: item.dosenRef,
								dosen: dosen ? dosen.name:''
							});
							cbb();
						}).catch(function(err){
							cbb(err);
						});
					}, function(err){
						if(err)
							cb(err);
						cb();
					});
				}).catch(function(err){
					cb(err);
				});
			},
			function(cb){
				Tugas.find().exec().then(function(data){
					async.forEach(data, function(item, cbb){
						var a = new Date(item.mulai);
						var b = new Date(item.selesai);
						var ddt = {
							_id : item._id,
							jenis : item.jenis,
							judul : item.judul,
							rincian : item.rincian,
							html_rincian: entities.decode(item.rincian),
							matakuliahRef : item.matakuliahRef,
							mulai : item.mulai==null ? '':moment(a).format('DD-MM-YYYY'),
							selesai : item.selesai==null ? '':moment(b).format('DD-MM-YYYY'),
							matakuliah : '',
							dosen : ''
						};

						if(item.matakuliahRef){
							Matakuliah.findOne({_id:item.matakuliahRef}).exec().then(function(mk){
								Profile.findOne({_id:mk.dosenRef}).exec().then(function(pr){
									ddt.matakuliah = mk ? mk.matakuliah:'';
									ddt.dosen = pr ? pr.name:'';
									params.data.push(ddt);
									cbb();
								}).catch(function(err){
									cbb(err);
								});
							}).catch(function(err){
								cbb();
							});
						}else{
							params.data.push(ddt);
							cbb();
						}
					}, function(err){
						if(err)
							cb(err);
						cb();
					});
				}).catch(function(err){
					cb(err);
				});
			}
		], function(err){
			if(err)
				next(err);
			params.random = random(1, 100);
			res.render('views/tugas', { title: 'Data Tugas', data: params });
		});
});

router.post('/save', function(req, res, next){
	var form_data = {
		jenis: req.body.jenis,
		judul: req.body.judul,
		rincian: req.body.rincian,
		matakuliahRef: req.body.matakuliahRef=='' ? null:req.body.matakuliahRef,
		mulai: breakDate(req.body.mulai),
		selesai: breakDate(req.body.selesai)
	};
	var tugas = new Tugas(form_data);
	tugas.save().then(function(data){
		res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/update', function(req, res, next){
	var id = req.body.id;
	Tugas.findOne({_id: id}).exec().then(function(tugas){
		if(tugas){
			tugas.jenis = req.body.jenis;
			tugas.judul = req.body.judul;
			tugas.rincian = req.body.rincian;
			tugas.matakuliahRef = req.body.matakuliahRef=='' ? null:req.body.matakuliahRef;
			tugas.mulai = breakDate(req.body.mulai);
			tugas.selesai = breakDate(req.body.selesai);
			tugas.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		Tugas.findOne({_id:id}).remove().exec();
	}
	res.send({status: true, message: 'Data deleted'});
});

module.exports = router;
