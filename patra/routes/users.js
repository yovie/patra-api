var express = require('express');
var router = express.Router();
var moment = require('moment');
var multer = require('multer');
var async = require('async');
var fs = require('fs');
var crypto = require('crypto');
var excelParser = require('excel-parser');

var Profile = require('../models/profile');
var Tmpprofile = require('../models/tmpprofile');
var User = require('../models/user');

var params = {
	module: 'data-pengguna'
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

var storage = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/profile');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname)
  	}
});

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      	position = position || 0;
      	return this.substr(position, searchString.length) === searchString;
  	};
}

var storageB = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/berkas');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname)
  	}
});

var upload = multer({storage: storage});
var uploadB = multer({storage: storageB});

router.get('/', function(req, res, next) {
	Profile.find().exec().then(function(data){
		var nwarr = new Array;
		for(id in data){
			var nwdata = new Object;
			for(fid in data[id].schema.paths){
				if(fid!='date_birth' && fid!='date_in' && fid!='date_out'){
					nwdata[fid] = data[id][fid];
				}
			}
			if(data[id].date_birth!=null){
				var a = new Date(data[id].date_birth);
				nwdata.date_birth = moment(a).format('DD-MM-YYYY');
			}else
				nwdata.date_birth = '';
			if(data[id].date_in!=null){
				var b = new Date(data[id].date_in);
				nwdata.date_in = moment(b).format('DD-MM-YYYY');
			}else
				nwdata.date_in = '';
			if(data[id].date_out!=null){
				var c = new Date(data[id].date_out);
				nwdata.date_out = moment(c).format('DD-MM-YYYY');
			}else
				nwdata.date_out = '';
			nwarr.push(nwdata);
		}
		params.data = nwarr;
		params.random = random(1, 100);
		res.render('views/users', { title: 'Data Pengguna', data: params });
	}).catch(function(err){
		next(err);
	});
});

router.post('/import', uploadB.single('berkas'), function(req, res, next){
	var createAccount = function(profile, cb){
		var user = new User();
		user.username = profile.name.replace(/\s+/g, '').toLowerCase();
		user.password = '12345';
		user.level = 'member';
		user.profileRef = profile._id;
		user.token = crypto.randomBytes(64).toString('hex');
		user.save().then(function(usr){
			profile.userRef = usr._id;
			profile.save().then(function(pro){
				cb();
			});
		});
	};
	var recs;
	async.waterfall([
			function(cb){
				excelParser.parse({
				  inFile: __dirname + '/../files/berkas/' + req.file.filename,
				  worksheet: 1,
				  skipEmpty: false
				},function(err, records){
				  if(err) 
				  	console.error(err);
				  else
				  	recs = records;
				  cb();
				});
			},

			function(cb){
				var icc = 0;
				console.log('total data = ' + recs.length);
				async.forEach(recs, function(item, cbb){
					if(icc==0){
						icc++;
						cbb();
					}else{
						var tmpP = new Tmpprofile();
						tmpP.ni = item[0];
						tmpP.name = item[2];
						var ttl = item[3].trim().split(",");
						var tempat = ttl[0];
						var tanggal = ttl[1].trim().split(" ");
						var dmonth;
						if(tanggal[1].toLowerCase().startsWith("jan"))
							dmonth = 1;
						else if(tanggal[1].toLowerCase().startsWith("feb"))
							dmonth = 2;
						else if(tanggal[1].toLowerCase().startsWith("mar"))
							dmonth = 3;
						else if(tanggal[1].toLowerCase().startsWith("apr"))
							dmonth = 4;
						else if(tanggal[1].toLowerCase().startsWith("mei"))
							dmonth = 5;
						else if(tanggal[1].toLowerCase().startsWith("jun"))
							dmonth = 6;
						else if(tanggal[1].toLowerCase().startsWith("jul"))
							dmonth = 7;
						else if(tanggal[1].toLowerCase().startsWith("agu"))
							dmonth = 8;
						else if(tanggal[1].toLowerCase().startsWith("sep"))
							dmonth = 9;
						else if(tanggal[1].toLowerCase().startsWith("okt"))
							dmonth = 10;
						else if(tanggal[1].toLowerCase().startsWith("nov"))
							dmonth = 11;
						else if(tanggal[1].toLowerCase().startsWith("des"))
							dmonth = 12;
						tmpP.date_birth = new Date(parseInt(tanggal[2]), dmonth-1, parseInt(tanggal[0]), 0,0,0,0);
						tmpP.place_birth = tempat;
						tmpP.address = item[9];
						tmpP.address_bandung = item[8];
						tmpP.email = item[7];
						tmpP.phone1 = item[4];
						tmpP.phone2 = item[5];
						tmpP.lineid = item[6];
						tmpP.angkatan = item[10].trim();
						tmpP.created_at = new Date();
						if(tmpP.date_birth==undefined){
							console.log(tanggal);
							console.log(dmonth);
							console.log(tmpP);
						}
						tmpP.save().then(function(pro){
							cbb();
						}).catch(function(err){
							console.log("ada error");
							console.log(err);
							// cbb();
						});
					}
				}, function(err){
					cb();
				});
			},

			function(cb){
				Tmpprofile.find().exec().then(function(recs){
					async.forEach(recs, function(item, cbb){
						var dain = new Date(parseInt(item.angkatan), 7, 1, 0,0,0,0);
						if(dain==undefined){
							console.log(item);
							cbb();
						}
						var profile = new Profile({
							ni: Number(item.ni).toString(),
							name: item.name,
							gender: 'lk',
							date_birth: item.date_birth,
							place_birth: item.place_birth,
							address: item.address,
							address_bandung: item.address_bandung,
							email: item.email,
							phone: item.phone1,
							phone2: item.phone2,
							position: 'anggota',
							date_in: dain,
							date_out: null,
							status: true,
							photo: 'user.png'
						});
						profile.save().then(function(usr){
							createAccount(usr, function(){
								cbb();
							})
						});
					}, function(err){
						cb();
					});	
				});
			},

			function(cb){
				Tmpprofile.find().remove().exec();
				cb();
			}
		], function(err){
			if(err)
				next(err);
			res.redirect('/' + params.module);
	});
});

router.post('/save', upload.single('photo'), function(req, res, next){
	var createAccount = function(profile, cb){
		var user = new User();
		user.username = profile.name.replace(/\s+/g, '').toLowerCase();
		user.password = '12345';
		user.level = 'member';
		user.profileRef = profile._id;
		user.token = crypto.randomBytes(64).toString('hex');
		user.save().then(function(usr){
			profile.userRef = usr._id;
			profile.save().then(function(pro){
				cb();
			});
		});
	};
	
	var form_data = {
		ni: req.body.ni,
		name: req.body.name,
		gender: req.body.gender,
		date_birth: breakDate(req.body.date_birth),
		place_birth: req.body.place_birth,
		address: req.body.address,
		email: req.body.email,
		phone: req.body.phone,
		position: req.body.position,
		date_in: breakDate(req.body.date_in),
		date_out: breakDate(req.body.date_out),
		status: true,
		photo: req.file==undefined?'user.png':req.file.filename
	};
	
	async.waterfall([
			function(cb){
				var profile = new Profile(form_data);
				profile.save().then(function(data){
					createAccount(data, function(){
						cb();
					});
				}).catch(function(err){
					next(err);
				});
			}
		], function(err){
			if(err)
				next(err);
			res.redirect('/' + params.module);
	});

});

router.post('/update', upload.single('photo'), function(req, res, next){
	var id = req.body.id;
	Profile.findOne({_id: id}).exec().then(function(profil){
		if(profil){
			profil.ni = req.body.ni;
			profil.name = req.body.name;
			profil.gender = req.body.gender;
			profil.date_birth = breakDate(req.body.date_birth);
			profil.place_birth = req.body.place_birth;
			profil.address = req.body.address;
			profil.email = req.body.email;
			profil.phone = req.body.phone;
			profil.position = req.body.position;
			profil.date_in = breakDate(req.body.date_in);
			profil.date_out = breakDate(req.body.date_out);
			if(req.file!=undefined)
				profil.photo = req.file.filename;
			profil.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

/*ajax*/
router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		async.waterfall([
			function(cb){
				Profile.findOne({_id:id}).exec().then(function(pro){
					if(pro){
						if(pro.photo!='user.png'){
							if(fs.existsSync(__dirname + '/../files/profile/' + pro.photo)) {
								fs.unlink(__dirname + '/../files/profile/' + pro.photo, function(err){
									cb();
								});
							}else
								cb();
						}else
							cb();
					}else
						cb();
				}).catch(function(err){
					cb(err);
				});
			},
			function(cb){
				Profile.find({_id:id}).remove().exec();
				cb();
			}
		], function(err){
			if(err)
				next(err);
			res.send({status: true, message: 'Data deleted'});
		});
	}else
		res.send({status: true, message: 'Data deleted'});
});

/*ajax*/
router.post('/disable', function(req, res, next){
	var id = req.body.id;
	var status = req.body.status;
	Profile.findOne({_id:id}).exec().then(function(profile){
		profile.status = status;
		profile.save().then(function(ori){
			User.findOne({_id:profile.userRef}).exec().then(function(usr){
				usr.status = status;
				usr.save().then(function(us){
					res.send({status: true, message: 'Disabled'});
				});
			});
		}).catch(function(err){
			next(err);
		});
	}).catch(function(err){
		next(err);
	})
});

/*ajax*/
router.get('/account/:id', function(req, res, next){
	var id = req.params.id;
	User.findOne({_id:id}).exec().then(function(acc){
		res.send({status: true, data: acc});
	}).catch(function(err){
		next(err);
	});
});

router.post('/account/update', function(req, res, next){
	var id = req.body.id;
	var username = req.body.username;
	var password1 = req.body.password1;
	var password2 = req.body.password2;

	User.findOne({_id:id}).exec().then(function(acc){
		acc.username = username;
		if(password1==password2 && password1.length>0){
			acc.password = password1.trim();
		}
		acc.save().then(function(us){
			res.redirect('/' + params.module);
		}).catch(function(err){
			next(err);
		});
	}).catch(function(err){
		next(err);
	});
});

router.post('/account/reset', function(req, res, next){
	var id = req.body.id;

	User.findOne({_id:id}).exec().then(function(acc){
		acc.password = '12345';
		acc.save().then(function(us){
			res.redirect('/' + params.module);
		}).catch(function(err){
			next(err);
		});
	}).catch(function(err){
		next(err);
	});
});

router.post('/pengguna', function(req, res, next){
	var start = 0;
	var length = 10;
	if(req.body.start!=undefined)
		start = req.body.start;
	if(req.body.length!=undefined)
		length = req.body.length;
	var allsize = length;
	var sEcho = 0;
	if(req.body.sEcho!=undefined)
		sEcho = req.body.sEcho;
	var searchValue = '';
	if(req.body['search[value]']!=undefined)
		searchValue = req.body['search[value]'];
	var oSortC = false;
	var oSortD = false;
	if(req.body['order[0][column]']!=undefined)
		oSortC = req.body['order[0][column]'];
	if(req.body['order[0][dir]']!=undefined)
		oSortD = req.body['order[0][dir]'];

	// console.log(req.body);

	var sFilter = {};
	if(searchValue.length>0){
		sFilter = {
			$or: [
				{ "ni": { "$regex": searchValue, "$options": "i" } },
				{ "name": { "$regex": searchValue, "$options": "i" } },
				{ "position": { "$regex": searchValue, "$options": "i" } }
			]
		};
	}
	var sOrder = {};
	if(oSortC){
		var ord = (oSortD=='asc') ? 1:-1;
		if(oSortC=='1')	sOrder = {ni: ord};
		if(oSortC=='2')	sOrder = {name: ord};
		if(oSortC=='3')	sOrder = {position: ord};
		if(oSortC=='5')	sOrder = {status: ord};
	}

	async.waterfall([
		function(cb){
			Profile.count(sFilter, function(err, c){
				allsize = c;
				cb();
			});
		},

		function(cb){
			Profile.find(sFilter).skip(start).limit(length).sort(sOrder).exec().then(function(data){
				var nwarr = new Array;
				for(id in data){
					var nwdata = new Object;
					for(fid in data[id].schema.paths){
						if(fid!='date_birth' && fid!='date_in' && fid!='date_out'){
							nwdata[fid] = data[id][fid];
						}
					}
					if(data[id].date_birth!=null){
						var a = new Date(data[id].date_birth);
						nwdata.date_birth = moment(a).format('DD-MM-YYYY');
					}else
						nwdata.date_birth = '';
					if(data[id].date_in!=null){
						var b = new Date(data[id].date_in);
						nwdata.date_in = moment(b).format('DD-MM-YYYY');
					}else
						nwdata.date_in = '';
					if(data[id].date_out!=null){
						var c = new Date(data[id].date_out);
						nwdata.date_out = moment(c).format('DD-MM-YYYY');
					}else
						nwdata.date_out = '';
					nwdata.nomor = parseInt(id)+parseInt(start)+1;
					nwarr.push(nwdata);
				}
				res.send({
					sEcho: sEcho,
					iTotalRecords: nwarr.length,
					iTotalDisplayRecords: allsize,
					aaData: nwarr
				});
				cb();
			}).catch(function(err){
				console.log(err);
				cb(err);
			});
		}
	], function(err){
		// do nothing
	});
});

module.exports = router;
