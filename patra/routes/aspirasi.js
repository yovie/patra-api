var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var Entities = require('html-entities').AllHtmlEntities;

var Aspirasi = require('../models/aspirasi');
var Profile = require('../models/profile');

var params = {
	module: 'aspirasi'
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

router.get('/', function(req, res, next) {
	params.data = new Array;
	entities = new Entities();
	async.parallel([
			function(cb){
				Aspirasi.find({deleted:false}).exec().then(function(data){
					async.forEach(data, function(item, cbb){
						Profile.findOne({_id:item.kontakRef}).exec().then(function(kontak){
							var a = new Date(item.tanggal);
							var fdata = {
								_id: item._id,
								judul: item.judul,
								detail: item.detail,
								html_detail: entities.decode(item.detail),
								tanggal: item.tanggal==null ? '':moment(a).format('DD-MM-YYYY'),
								kontakRef: item.kontakRef,
								kontak: ''
							};
							if(kontak)
								fdata.kontak = kontak.name;
							params.data.push(fdata);
							cbb();
						}).catch(function(err){
							cbb(err);
						});
					}, function(err){
						if(err)
							cb(err);
						cb();
					});
				}).catch(function(err){
					cb(err);
				});
			}
		], function(err){
			if(err)
				next(err);
			params.random = random(1, 100);
			res.render('views/aspirasi', { title: 'Data Aspirasi', data: params });
		});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	Aspirasi.findOne({_id: id}).exec().then(function(asp){
		if(asp){
			asp.deleted = true;
			asp.save().then(function(data){
				res.send({status: true, message: 'Data deleted'});
			}).catch(function(err){
				next(err);
			});
		}else
			res.send({status: true, message: 'Data deleted'});
	}).catch(function(err){
		next(err);
	});
});

module.exports = router;
