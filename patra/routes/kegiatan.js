var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var Entities = require('html-entities').AllHtmlEntities;
var multer = require('multer');

var Kegiatan = require('../models/kegiatan');
var Profile = require('../models/profile');

var params = {
	module: 'kegiatan-patra'
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

var storage = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/kegiatan');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname);
  	}
});

var upload = multer({storage: storage});


router.get('/', function(req, res, next) {
	params.data = new Array;
	entities = new Entities();
	async.parallel([
			function(cb){
				Kegiatan.find().exec().then(function(data){
					async.forEach(data, function(item, cbb){
						Profile.findOne({_id:item.kontakRef}).exec().then(function(kontak){
							var a = new Date(item.pelaksanaan);
							var b = new Date(item.sampai);
							var fdata = {
								_id: item._id,
								nama: item.nama,
								deskripsi: entities.decode(item.deskripsi),
								html_deskripsi: entities.decode(item.deskripsi),
								pelaksanaan: item.pelaksanaan==null ? '':moment(a).format('DD-MM-YYYY'),
								sampai: item.sampai==null ? '':moment(b).format('DD-MM-YYYY'),
								kontakRef: item.kontakRef,
								kontak: '',
								gambar: item.gambar,
								yes: 0,
								no: 0,
								maybe: 0
							};
							if(kontak)
								fdata.kontak = kontak.name;
							if(Array.isArray(item.attendance)){
								for(let n=0; n<item.attendance.length; n++){
									if(item.attendance[n].status=='yes')
										fdata.yes++;
									if(item.attendance[n].status=='no')
										fdata.no++;
									if(item.attendance[n].status=='maybe')
										fdata.maybe++;
								}
							}
							params.data.push(fdata);
							cbb();
						}).catch(function(err){
							cbb(err);
						});
					}, function(err){
						if(err)
							cb(err);
						cb();
					});
				}).catch(function(err){
					cb(err);
				});
			},
			function(cb){
				Profile.find({position: 'anggota'}).exec().then(function(data){
					params.penanggungjawab = data;
					cb();
				}).catch(function(err){
					cb(err);
				});
			}
		], function(err){
			if(err)
				next(err);
			params.random = random(1, 100);
			res.render('views/kegiatan', { title: 'Data kegiatan PATRA', data: params });
		});
});

router.post('/save', upload.single('gambar'), function(req, res, next){
	var form_data = {
		nama: req.body.nama,
		deskripsi: req.body.deskripsi,
		pelaksanaan: breakDate(req.body.pelaksanaan),
		sampai: breakDate(req.body.sampai),
		kontakRef: req.body.kontakRef==''? null:req.body.kontakRef,
		gambar: req.file==undefined?'':req.file.filename
	};
	var kegiatan = new Kegiatan(form_data);
	kegiatan.save().then(function(data){
		res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/update', upload.single('gambar'), function(req, res, next){
	var id = req.body.id;
	Kegiatan.findOne({_id: id}).exec().then(function(keg){
		if(keg){
			keg.nama = req.body.nama;
			keg.deskripsi = req.body.deskripsi;
			keg.pelaksanaan = breakDate(req.body.pelaksanaan);
			keg.sampai = breakDate(req.body.sampai);
			if(req.body.kontakRef!='')
				keg.kontakRef = req.body.kontakRef;
			if(req.file!=undefined)
				keg.gambar = req.file.filename;
			keg.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		Kegiatan.find({_id:id}).remove().exec();
	}
	res.send({status: true, message: 'Data deleted'});
});

router.post('/attendance', function(req, res, next){
	var ids = req.body.ids;
	var rez = new Array;
	var start = 0;
	var length = 10;
	if(req.body.start!=undefined)
		start = req.body.start;
	if(req.body.length!=undefined)
		length = req.body.length;
	var allsize = length;
	var sEcho = 0;
	if(req.body.sEcho!=undefined)
		sEcho = req.body.sEcho;
	var sii = 0;

	Kegiatan.findOne({_id:ids}).exec().then(function(keg){
		if(keg!=null){
			if(Array.isArray(keg.attendance)){
				async.forEachSeries(keg.attendance, function(item, cb){
					Profile.findOne({userRef:item.member}).exec().then(function(pm){
						if(pm!=null){
							sii++;
							var mm = {
								no: sii,
								ni: pm.ni,
								name: pm.name,
								gender: pm.gender,
								photo: pm.photo,
								status: item.status,
								date: item.confirm
							}
							rez.push(mm);
						}
						cb();
					});
				}, function(err){
					res.send({
						sEcho: sEcho,
						iTotalRecords: rez.length,
						iTotalDisplayRecords: keg.attendance.length,
						aaData: rez
					});
				});
			}else
				res.send({
					sEcho: sEcho,
					iTotalRecords: 0,
					iTotalDisplayRecords: 0,
					aaData: []
				});
		}else
			res.send({
				sEcho: sEcho,
				iTotalRecords: 0,
				iTotalDisplayRecords: 0,
				aaData: []
			});
	});
});

module.exports = router;
