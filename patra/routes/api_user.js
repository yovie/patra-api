var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var fs = require('fs');
var Entities = require('html-entities').AllHtmlEntities;
var _ = require('underscore');
const mongoose = require('mongoose');

var Profile = require('../models/profile');
var Jadwal = require('../models/jadwal');
var Tugas = require('../models/tugas');
var Kegiatan = require('../models/kegiatan');
var Timeline = require('../models/timeline');
var Matakuliah = require('../models/matakuliah');
var Informasi = require('../models/informasi');
var Arsip = require('../models/arsip');
var Kuisioner = require('../models/kuisioner');
var KuisionerDetail = require('../models/kuisioner_detail');
var KuisionerUser = require('../models/kuisioner_user');
var Aspirasi = require('../models/aspirasi');
var User = require('../models/user');

var params = {
	bulan : [ 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' ]
};

var idxHari = {
	senin: 0, selasa: 1, rabu: 2, kamis: 3, jumat: 4, sabtu: 5, minggu: 6
};

function gm(col) {
	if (col == 'profile') return Profile;
	if (col == 'jadwal') return Jadwal;
	if (col == 'tugas') return Tugas;
	if (col == 'kegiatan') return Kegiatan;
	if (col == 'timeline') return Timeline;
	if (col == 'informasi') return Informasi;
	if (col == 'arsip') return Arsip;
	if (col == 'kuisioner') return Kuisioner;
	if (col == 'kuisioner_detail') return KuisionerDetail;
	if (col == 'kuisioner_user') return KuisionerUser;
	if (col == 'aspirasi') return Aspirasi;
	if (col == 'matakuliah') return Matakuliah;
	return null;
}

// access with http://localhost:8000/api?access_token=86b801f9771a3027877604d472ee7bf7651ad4f42aa09e05ca43735f67fcd7599ab40e469639e6595c8c9ba53391a022dc2f4d64dfa852ac35293e16247c4eec
router.get('/', function(req, res, next) {
	if (req.user) {
		var upro = {
			id : req.user._id,
			username : req.user.username,
			token : req.user.token,
			status : req.user.status,
			email : req.user.email
		}
		res.send({
			result : true,
			user : upro
		});
	} else
		res.send({
			result : false
		});
});

router.post('/data', function(req, res, next) {
	var source = req.body.source;
	var skip = req.body.skip;
	var offset = req.body.offset;
	var filterF = req.body.filterF;
	var filterV = req.body.filterV;
	var collection = '';
	var sort;
	if (source == 'p')
		collection = 'profile';
	if (source == 'j')
		collection = 'jadwal';
	if (source == 't')
		collection = 'tugas';
	if (source == 'k')
		collection = 'kegiatan';
	if (source == 'ti')
		collection = 'timeline';
	if (source == 'i')
		collection = 'informasi';
	if (source == 'a')
		collection = 'arsip';
	if (source == 'k')
		collection = 'kuisioner';
	if (source == 'kd')
		collection = 'kuisioner_detail';
	if (source == 'ku')
		collection = 'kuisioner_user';
	if (source == 'as'){
		collection = 'aspirasi';
		sort = {tanggal: 1};
	}

	var model = gm(collection);
	if (model == null)
		res.send({
			status : false,
			data : []
		});

	var filter = {};
	if (Array.isArray(filterF)) {
		for (fit in filterF) {
			filter[filterF[fit]] = filterV[fit];
		}
	}

	var formatter = function(rs, dt, col) {
		hasil = new Array;
		async.forEachSeries(dt, function(item, cbb) {
			var asd = new Array;
			if (col == 'aspirasi') {
				var a = new Date(item.tanggal);
				var tgl_text = moment(a).format('DD-MM-YYYY');
				asd = {
					_id : item._id,
					judul : item.judul,
					detail : item.detail,
					tanggal : tgl_text,
					kontakRef : item.kontakRef
				};
			}
			hasil.push(asd);
			cbb();
		}, function(err) {
			if (err)
				rs.send({
					status : false,
					data : []
				});
			else
				rs.send({
					status : true,
					data : hasil
				});
		});
	}

	if (skip != undefined && offset != undefined) {
		model.find(filter).skip(skip).offset(offset).exec().then(function(rs) {
			formatter(res, rs, collection);
		}).catch(function(err) {
			res.send({
				status : false
			});
		});
	} else if (skip != undefined) {
		model.find(filter).skip(skip).exec().then(function(rs) {
			formatter(res, rs, collection);
		}).catch(function(err) {
			res.send({
				status : false
			});
		});
	} else if (offset != undefined) {
		model.find(filter).offset(offset).exec().then(function(rs) {
			formatter(res, rs, collection);
		}).catch(function(err) {
			res.send({
				status : false
			});
		});
	} else if (sort != undefined) {
		model.find(filter).sort(sort).exec().then(function(rs) {
			formatter(res, rs, collection);
		}).catch(function(err) {
			res.send({
				status : false
			});
		});
	} else {
		model.find(filter).exec().then(function(rs) {
			formatter(res, rs, collection);
		}).catch(function(err) {
			res.send({
				status : false
			});
		});
	}
});


router.post('/save', function(req, res, next) {
	var dest = req.body.destination;
	var collection = '';
	if (dest == 'as')
		collection = 'aspirasi';
	if (dest == 'ku')
		collection = 'kuisioner_user';

	var model = gm(collection);
	if (model == null)
		res.send({
			status : false
		});

	if (collection == 'aspirasi') {
		var formdata = {
			kontakRef : req.user.profileRef,
			judul : req.body.judul,
			detail : req.body.detail,
			tanggal : new Date
		}
		var aspirasi = new Aspirasi(formdata);
		aspirasi.save().then(function(asp) {
			res.send({
				status : true,
				data : asp
			});
		}).catch(function(err) {
			res.send({
				status : false
			});
		});
	} else if (collection == 'kuisioner_user') {
		var formdata = {
			kuisionerRef : req.body.param1,
			kontakRef : req.body.param2,
			choiceValue : req.body.param3,
			textValue : req.body.param4,
			created_at : new Date
		}
		var kuis = new KuisionerUser(formdata);
		kuis.save().then(function(asp) {
			res.send({
				status : true,
				data : asp
			});
		}).catch(function(err) {
			res.send({
				status : false
			});
		});
	} else
		res.send({
			status : false
		});
});

router.post('/kuisioner', function(req, res, next) {
	gm('kuisioner').find({
		status : 'open'
	}).sort({judul: 1}).exec().then(function(has) {
		res.send({
			status : true,
			data : has
		});
	}).catch(function(err) {
		res.send({
			status : false
		});
	})
});

router.post('/kuisioner_detail', function(req, res, next) {
	var data = new Array;
	var tmp = new Array;
	var kuisid = req.body.kuisid;
	var uid = req.body.uid;
	async.waterfall([
		function(cb) {
			gm('kuisioner_detail').find({
				kuisionerRef : kuisid
			}).sort({tipe: 1}).exec().then((rez) => {
				tmp = rez;
				cb();
			});
		},
		function(cb) {
			async.forEachSeries(tmp, (item, cbb) => {
				gm('kuisioner_user').findOne({
					kuisionerDetailRef : item._id,
					kontakRef : uid
				}).exec().then((rez) => {
					data.push({
						_id : item._id,
						kuisionerRef : item.kuisionerRef,
						pertanyaan : item.pertanyaan,
						tipe : item.tipe,
						choice : item.choice,

						kontakRef : (rez!=null) ? rez.kontakRef : '',
						choiceValue : (rez!=null) ? rez.choiceValue : '',
						textValue : (rez!=null) ? rez.textValue : ''
					});
					cbb();
				})
			}, (err) => {
				// console.log(data);
				cb();
			});
		}
	], function(err) {
		if (err)
			res.send({
				status : false
			});
		else
			res.send({
				status : true,
				data : data
			});
	});
});

router.post('/kuisioner_post', function(req, res, next) {
	var kuisdetailid = req.body.kuisdetailid;
	var uid = req.body.uid;
	var tipe = req.body.tipe;
	var value = req.body.value;
	var formdata;
	if (tipe == 'choice') {
		formdata = {
			kuisionerDetailRef : kuisdetailid,
			tipe: tipe,
			kontakRef : uid,
			choiceValue : value,
			created_at : new Date(),
		}
	} else {
		formdata = {
			kuisionerDetailRef : kuisdetailid,
			tipe: tipe,
			kontakRef : uid,
			textValue : value,
			created_at : new Date(),
		}
	}
	gm('kuisioner_user').findOne({kuisionerDetailRef: kuisdetailid, kontakRef: uid}).exec().then((rez)=>{
		if(rez){
			if(rez.tipe=='choice'){
				rez.choiceValue = value;
				rez.updated_at = new Date;
			}else{
				rez.textValue = value;
				rez.updated_at = new Date;
			}
			rez.save().then((kuser)=>{
				if(kuser)
					res.send({status:true, data: kuser});
				else
					res.send({status:false});
			}).catch((err)=>{
				res.send({status:false, error: err});
			});	
		}else{
			var newData = new KuisionerUser(formdata);
			newData.save().then((kuser)=>{
				if(kuser)
					res.send({status:true, data: kuser});
				else
					res.send({status:false});
			}).catch((err)=>{
				res.send({status:false, error: err});
			});	
		}
	});
});

router.post('/data/jadwal', function(req, res, next){
	tmps = new Array;
	daftar = new Array;
	var angkatan = 2013;

	async.waterfall([	
		function(cb){
			gm('profile').findOne({_id:req.user.profileRef}).exec().then( data => {
				if(data){
					angkatan = data.date_in.getFullYear();
				}
				cb();
			});
		},
		function(cb){
			gm('jadwal').find({angkatan: angkatan}).sort({jammulai:1}).exec().then(function(data){
				async.forEachSeries(data, function(item, cbb){
					if(item.matakuliahRef){
						gm('matakuliah').findOne({_id:item.matakuliahRef}).exec().then(function(matkul){
							tmps.push({
								_id: item._id,
								hari: item.hari,
								jam_mulai: item.jam_mulai,
								jam_selesai: item.jam_selesai,
								matakuliahRef: item.matakuliahRef,
								matakuliah: matkul ? matkul.matakuliah:'',
								matakuliah_kode: matkul ? matkul.kode:'',
								gambar: matkul ? matkul.gambar:'',
								dosenRef: matkul ? matkul.dosenRef:''
							});
							cbb();
						}).catch(function(err){
							// cbb();
							console.log(err);
						});
					}else{
						tmps.push({
							_id: item._id,
							hari: item.hari,
							jam_mulai: item.jam_mulai,
							jam_selesai: item.jam_selesai,
							matakuliahRef: item.matakuliahRef,
							matakuliah: '',
							matakuliah_kode: '',
							gambar: '',
							dosenRef: ''
						});
						cbb();
					}
				}, function(err){
					if(err){
						console.log(err);
						cb();
					}else
						cb();
				});
			}).catch(function(err){
				next(err);
			});
		},
		function(cb){
			async.forEachSeries(tmps, function(item, cbb){
				// if(item.dosenRef){
				if(mongoose.Types.ObjectId.isValid(item.dosenRef)){
					gm('profile').findOne({_id:item.dosenRef}).exec().then(function(dosen){
						item.nama_dosen = dosen?dosen.name:'';
						item.ni = dosen?dosen.ni:'';
						daftar.push(item);
						cbb();
					}).catch(function(err){
						cbb();
						console.log(err);
					});
				}else{
					// item.nama_dosen = '';
					// item.ni = '';
					// daftar.push(item);
					cbb();
				}
			}, function(err){
				if(err){
					console.log(err);
					cb();
				}else
					cb();
			});
		},
		function(cb){
			// daftarSorter = daftar.slice(0);
			daftar.sort(function(a, b){
				return  idxHari[a.hari]-idxHari[b.hari];
			});
			cb();
		}
	], function(err){
		if(err)
			res.send({status:false, data: []});
		else
			res.send({status:true, data: daftar});
	});
});

router.post('/kegiatan_confirmation_post', function(req, res, next) {
	var kegiatanid = req.body.kegiatanid;
	var confirmation = req.body.confirmation;
	gm('kegiatan').findOne({_id: kegiatanid}).exec().then((keg)=>{
		if(keg){
			var attendance = new Array;
			if(Array.isArray(keg.attendance)){
				for(i=0;i<keg.attendance.length;i++){
					if(!keg.attendance[i].member.equals(req.user._id)){
						attendance.push(keg.attendance[i]);
					}
				}
			}
			attendance.push({
				member: req.user._id,
				status: confirmation,
				confirm: new Date()
			});
			keg.attendance = attendance;
			// console.log(keg);	
			keg.save().then((item)=>{
				var a = new Date(item.pelaksanaan);
				var b = new Date(item.sampai);
				var nw = new Date();
				var status = '';
				if(Array.isArray(item.attendance)){
					for(i=0;i<item.attendance.length;i++){
						if(item.attendance[i].member.equals(req.user._id)){
							status = item.attendance[i].status;
						}
					}
				}
				var fdata = {
					_id: item._id,
					nama: item.nama,
					deskripsi: item.deskripsi,
					pelaksanaan: item.pelaksanaan==null ? '':moment(a).format('DD-MM-YYYY'),
					sampai: item.sampai==null ? '':moment(b).format('DD-MM-YYYY'),
					kontakRef: item.kontakRef,
					gambar: item.gambar,
					status: status,
					kontak: '',
					expired: false
				};
				if(item.pelaksanaan!=null){
					if(nw>=item.pelaksanaan)
						fdata.expired = true;
				}
				// console.log(fdata);
				res.send({status:true, data: fdata});	
			});
		}else{
			res.send({status:false});
		}
	});
});

router.post('/data/kegiatan', function(req, res, next){
	daftar = new Array;
	async.parallel([
		function(cb){
			gm('kegiatan').find().sort({pelaksanaan: 1}).exec().then(function(data){
				async.forEachSeries(data, function(item, cbb){

					var status = '';
					if(Array.isArray(item.attendance)){
						for(i=0;i<item.attendance.length;i++){
							if(item.attendance[i].member.equals(req.user._id)){
								status = item.attendance[i].status;
							}
						}
					}
					gm('profile').findOne({_id:item.kontakRef}).exec().then(function(kontak){
						var a = new Date(item.pelaksanaan);
						var b = new Date(item.sampai);
						var nw = new Date();
						var fdata = {
							_id: item._id,
							nama: item.nama,
							deskripsi: item.deskripsi,
							pelaksanaan: item.pelaksanaan==null ? '':moment(a).format('DD-MM-YYYY'),
							sampai: item.sampai==null ? '':moment(b).format('DD-MM-YYYY'),
							kontakRef: item.kontakRef,
							gambar: item.gambar,
							status: status,
							kontak: '',
							expired: false
						};
						if(item.pelaksanaan!=null){
							if(nw>=item.pelaksanaan)
								fdata.expired = true;
						}
						if(kontak)
							fdata.kontak = kontak.name;
						daftar.push(fdata);
						cbb();
					}).catch(function(err){
						console.log(err);
					});

				}, function(err){
					if(err)
						console.log(err);
					cb();
				});
			}).catch(function(err){
				console.log(err);
			});
		}
	], function(err){
		// console.log(daftar);
		if(err)
			res.send({status:false, data: []});
		else
			res.send({status:true, data: daftar});
	});
});


router.post('/change-username', function(req, res, next){
	var oldpwd = req.body.oldpwd;
	var newuname = req.body.newuname;
	if(!oldpwd)
		return res.send({status:false, message:'Password lama tidak valid'});
	if(!newuname)
		return res.send({status:false, message:'Username baru tidak valid'});
	if(newuname.length<6)
		return res.send({status:false, message:'Username harus lebih dari lima karakter'});
	User.findOne({_id:req.user._id, password: oldpwd}).exec().then((usr)=>{
		if(usr){
			User.findOne({username: newuname, _id:{$ne:req.user._id}}).exec().then((us)=>{
				if(us){
					res.send({status:false, message:'Username sudah digunakan'});
				}else{
					usr.username = newuname;
					usr.save().then((usn)=>{
						res.send({status:true, message:'Username sudah diubah'});	
					}).catch((err)=>{
						console.log('error on save', err);
						res.send({status:false, message:'Terjadi kesalahan dalam proses'});		
					});
				}
			}).catch((err)=>{
				console.log('error on find user', err);
				res.send({status:false, message:'Terjadi kesalahan dalam proses'});		
			});
		}else{
			res.send({status:false, message:'User tidak dikenali'});	
		}
	}).catch((err)=>{
		console.log('error on find account', err);
		res.send({status:false, message:'Terjadi kesalahan dalam proses'});
	});
});

router.post('/change-password', function(req, res, next){
	var oldpwd = req.body.oldpwd;
	var newpwd = req.body.newpwd;
	var newpwdconfirm = req.body.newpwdconfirm;
	if(!oldpwd)
		return res.send({status:false, message:'Password lama tidak valid'});
	if(!newpwd)
		return res.send({status:false, message:'Password baru tidak valid'});
	if(newpwd.length<6)
		return res.send({status:false, message:'Password baru harus lebih dari lima karakter'});
	if(!newpwdconfirm)
		return res.send({status:false, message:'Password konfirmasi tidak valid'});
	if(newpwdconfirm.length<6)
		return res.send({status:false, message:'Password konfirmasi harus lebih dari lima karakter'});
	if(newpwdconfirm!=newpwd)
		return res.send({status:false, message:'Password konfirmasi tidak sama'});
	User.findOne({_id:req.user._id, password: oldpwd}).exec().then((usr)=>{
		if(usr){
			usr.password = newpwd;
			usr.save().then((usn)=>{
				res.send({status:true, message:'Password sudah diubah'});	
			}).catch((err)=>{
				res.send({status:false, message:'Terjadi kesalahan dalam proses'});		
			});
		}else{
			res.send({status:false, message:'User tidak dikenali'});	
		}
	}).catch((err)=>{
		res.send({status:false, message:'Terjadi kesalahan dalam proses'});
	});
});

router.get('/angkatan', function(req, res, next){
	gm('profile').find({position:'anggota'}).sort({date_in:1}).exec().then(function(mh){
		var tahun = new Array;
		for(imh in mh){
			var din = mh[imh].date_in;
			if(din!=null){
				if(!_.contains(tahun, din.getFullYear()) && (din.getFullYear()>2010))
					tahun.push(din.getFullYear());
			}
		}
		console.log('angkatan', tahun);
		res.send({status:true, angkatan:tahun});
	}).catch(function(err){
		console.log('error');
		res.send({status:false, angkatan:[]});
	});
});

router.post('/data/tugasx', function(req, res, next){
	matkul = new Array;
	tugas = new Array;
	jadwal = new Array;
	angkatan = 0;

	async.waterfall([
		function(cb){
			gm('profile').findOne({_id:req.user.profileRef}).exec().then( data => {
				if(data){
					angkatan = data.date_in.getFullYear();
				}
				cb();
			});
		},
		function(cb){
			gm('jadwal').find({angkatan:angkatan}).exec().then( jdw => {
				if(jdw){
					jadwal = jdw;	
				}
				cb();
			});
		},
		function(cb){
			async.forEachSeries(jadwal, function(item, cbb){
				var ada = false;
				for(mm in matkul){
					// console.log(matkul[mm]._id,item.matakuliahRef);
					// console.log(matkul[mm]._id.equals(item.matakuliahRef));
					if(matkul[mm]._id.equals(item.matakuliahRef))
						ada = true;
				}
				if(!ada){
					gm('matakuliah').findOne({_id:item.matakuliahRef}).exec().then((data)=>{
						if(data)
							matkul.push(data);
						cbb();
					});
				}else
					cbb();
			}, function(err){
				cb();
			});
		},
		// function(cb){
		// 	gm('matakuliah').find().exec().then((data)=>{
		// 		matkul = data;
		// 		cb();
		// 	});
		// },
		function(cb){
			// console.log(matkul);
			async.forEachSeries(matkul, function(item, cbb){
				gm('tugas').find({matakuliahRef:item._id}).exec().then((data)=>{
					if(data){

						gm('profile').findOne({_id:item.dosenRef}).exec().then(function(pr){
							namadosen = pr ? pr.name:'';
							tugasnya = new Array;

							for(ii in data){
								var a = new Date(data[ii].mulai);
								var b = new Date(data[ii].selesai);
								tugasnya.push({
									mulai: moment(a).format('DD-MM-YYYY'),
									jenis: data[ii].jenis,
									matakuliahRef: data[ii].matakuliahRef,
									judul: data[ii].judul,
									rincian: data[ii].rincian,
									selesai: moment(b).format('DD-MM-YYYY'),
									dosen: namadosen
								});
							}
							tugas.push({
								kode : item.kode,
								matakuliah : item.matakuliah,
								sks : item.sks,
								dosenRef : item.dosenRef,
								gambar : item.gambar,
								tugas : tugasnya
							});
							
							cbb();
						}).catch(function(err){
							cbb(err);
						});

					}else
						cbb();
				}).catch(function(err){
					// console.log(err);
					cbb();
				});
			}, function(err){
				console.log('bada');
				cb();
			});
		}
	], function(err){
		// console.log(tugas);
		if(err)
			res.send({status:false});
		else
			res.send({status:true, data: tugas});
	});
});

module.exports = router;