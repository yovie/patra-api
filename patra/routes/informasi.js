var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var Entities = require('html-entities').AllHtmlEntities;
var multer = require('multer');

var Informasi = require('../models/informasi');

var params = {
	module: 'informasi-bulanan',
	refbulan: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

function createDate(bln, thn){
	// console.log(thn, bln);
	if(bln==undefined||thn==undefined)
		return null;
	var dt = new Date(parseInt(thn), parseInt(bln)-1, 1, 0, 0, 0, 0);
	// console.log('datena', dt);
	return dt;	
}

var storage = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/info');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname);
  	}
});

var upload = multer({storage: storage});

router.get('/', function(req, res, next) {
	params.data = new Array;
	params.random = random(1, 100);
	entities = new Entities();
	Informasi.find().exec().then(function(data){
		async.forEach(data, function(item, cbb){
			var dt = item.bulan;
			var itahun;
			var ibulan;
            if(dt!=null && dt!=undefined){
            	itahun = dt.getFullYear();
            	ibulan = dt.getMonth();
            }
			params.data.push({
				_id: item._id,
				judul: item.judul,
				detail: item.detail,
				html_detail: entities.decode(item.detail),
				tahun: parseInt(itahun),
				bulan: parseInt(ibulan)+1,
				gambar: item.gambar
			});
			cbb();
		}, function(err){
			if(err)
				cb(err);
			res.render('views/informasi', { title: 'Informasi Bulanan', data: params });
		});
	}).catch(function(err){
		next(err);
	})
});

router.post('/save', upload.single('gambar'), function(req, res, next){
	var form_data = {
		judul: req.body.judul,
		detail: req.body.detail,
		bulan: createDate(req.body.bulan, req.body.tahun),
		gambar: req.file==undefined?'':req.file.filename
	};
	var informasi = new Informasi(form_data);
	informasi.save().then(function(data){
		res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/update', upload.single('gambar'), function(req, res, next){
	var id = req.body.id;
	Informasi.findOne({_id: id}).exec().then(function(tm){
		if(tm){
			tm.judul = req.body.judul;
			tm.detail = req.body.detail;
			tm.bulan = createDate(req.body.bulan, req.body.tahun);
			if(req.file!=undefined)
				tm.gambar = req.file.filename;
			tm.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		Informasi.find({_id:id}).remove().exec();
	}
	res.send({status: true, message: 'Data deleted'});
});

module.exports = router;
