var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var fs = require('fs');
var Entities = require('html-entities').AllHtmlEntities;

var _ = require('underscore');

var Profile = require('../models/profile');
var Jadwal = require('../models/jadwal');
var Tugas = require('../models/tugas');
var Kegiatan = require('../models/kegiatan');
var Timeline = require('../models/timeline');
var Matakuliah = require('../models/matakuliah');
var Informasi = require('../models/informasi');
var Arsip = require('../models/arsip');
var Kuisioner = require('../models/kuisioner');
var KuisionerDetail = require('../models/kuisioner_detail');
var KuisionerUser = require('../models/kuisioner_user');
var Aspirasi = require('../models/aspirasi');

var params = {
	bulan: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
};

var idxHari = {
	senin: 0, selasa: 1, rabu: 2, kamis: 3, jumat: 4, sabtu: 5, minggu: 6
};

function gm(col){
	if(col=='profile') return Profile;
	if(col=='jadwal') return Jadwal;
	if(col=='tugas') return Tugas;
	if(col=='kegiatan') return Kegiatan;
	if(col=='timeline') return Timeline;
	if(col=='informasi') return Informasi;
	if(col=='arsip') return Arsip;
	if(col=='kuisioner') return Kuisioner;
	if(col=='kuisioner_detail') return KuisionerDetail;
	if(col=='kuisioner_user') return KuisionerUser;
	if(col=='aspirasi') return Aspirasi;
	if(col=='matakuliah') return Matakuliah;
	return null;
}

// access with http://localhost:8000/api?access_token=86b801f9771a3027877604d472ee7bf7651ad4f42aa09e05ca43735f67fcd7599ab40e469639e6595c8c9ba53391a022dc2f4d64dfa852ac35293e16247c4eec
router.get('/', function(req, res, next) {
	if(req.user){
		var upro = {
			id: req.user._id,
			username:  req.user.username,
			token: req.user.token,
			status: req.user.status,
			email: req.user.email
		}
		res.send({result:true, user: upro});
	}else
		res.send({result:false});
});

router.post('/data', function(req, res, next) {
	var source = req.body.source;
	var skip = req.body.skip;
	var offset = req.body.offset;
	var filterF = req.body.filterF;
	var filterV = req.body.filterV;
	var collection = '';
	var sort;
	if(source=='p') collection = 'profile';
	if(source=='j') collection = 'jadwal';
	if(source=='t') collection = 'tugas';
	if(source=='k') collection = 'kegiatan';
	if(source=='ti') collection = 'timeline';
	if(source=='i') collection = 'informasi';
	if(source=='a'){ 
		collection = 'arsip';
		sort = {nama: 1};
	}
	if(source=='k') collection = 'kuisioner';
	if(source=='kd') collection = 'kuisioner_detail';
	if(source=='ku') collection = 'kuisioner_user';
	if(source=='as') collection = 'aspirasi';

	var model = gm(collection);
	if(model==null)
		res.send({status: false});

	var filter = {};
	if(filterF)
		filter = {filterF: filterV};

	if(skip!=undefined && offset!=undefined){
		model.find(filter).skip(skip).offset(offset).exec().then(function(rs){
			res.send({status: true, data: rs});
		}).catch(function(err){
			res.send({status: false});
		});
	}else if(skip!=undefined){
		model.find(filter).skip(skip).exec().then(function(rs){
			res.send({status: true, data: rs});
		}).catch(function(err){
			res.send({status: false});
		});
	}else if(offset!=undefined){
		model.find(filter).offset(offset).exec().then(function(rs){
			res.send({status: true, data: rs});
		}).catch(function(err){
			res.send({status: false});
		});
	}else if(sort!=undefined){
		model.find(filter).sort(sort).exec().then(function(rs){
			res.send({status: true, data: rs});
		}).catch(function(err){
			res.send({status: false});
		});
	}else{
		model.find(filter).exec().then(function(rs){
			res.send({status: true, data: rs});
		}).catch(function(err){
			res.send({status: false});
		});
	}
});

router.get('/data/jadwal', function(req, res, next){
	tmps = new Array;
	daftar = new Array;
	var angkatan = 2015;
	if(req.query.angkatan)
		angkatan = req.query.angkatan;
	// daftarSorter = new Array;

	async.waterfall([		
		function(cb){
			gm('jadwal').find({angkatan: angkatan}).exec().then(function(data){
				async.forEach(data, function(item, cbb){
					if(item.matakuliahRef){
						gm('matakuliah').findOne({_id:item.matakuliahRef}).exec().then(function(matkul){
							tmps.push({
								_id: item._id,
								hari: item.hari,
								jam_mulai: item.jam_mulai,
								jam_selesai: item.jam_selesai,
								matakuliahRef: item.matakuliahRef,
								matakuliah: matkul ? matkul.matakuliah:'',
								matakuliah_kode: matkul ? matkul.kode:'',
								dosenRef: matkul ? matkul.dosenRef:''
							});
							cbb();
						}).catch(function(err){
							cbb(err);
						});
					}else{
						tmps.push({
							_id: item._id,
							hari: item.hari,
							jam_mulai: item.jam_mulai,
							jam_selesai: item.jam_selesai,
							matakuliahRef: item.matakuliahRef,
							matakuliah: '',
							matakuliah_kode: '',
							dosenRef: ''
						});
						cbb();
					}
				}, function(err){
					if(err){
						console.log(err);
						cb(err);
					}else
						cb();
				});
			}).catch(function(err){
				next(err);
			});
		},
		function(cb){
			async.forEach(tmps, function(item, cbb){
				if(item.dosenRef){
					gm('profile').findOne({_id:item.dosenRef}).exec().then(function(dosen){
						item.nama_dosen = dosen?dosen.name:'';
						item.ni = dosen?dosen.ni:'';
						daftar.push(item);
						cbb();
					}).catch(function(err){
						cbb(err);
					});
				}else{
					item.nama_dosen = '';
					item.ni = '';
					daftar.push(item);
					cbb();
				}
			}, function(err){
				if(err){
					console.log(err);
					cb(err);
				}else
					cb();
			});
		},
		function(cb){
			// daftarSorter = daftar.slice(0);
			daftar.sort(function(a, b){
				return  idxHari[a.hari]-idxHari[b.hari];
			});
			cb();
		}
	], function(err){
		if(err)
			res.send({status:false, data: []});
		else
			res.send({status:true, data: daftar});
	});
});

router.get('/data/tugas', function(req, res, next){
	daftar = new Array;

	async.parallel([
		function(cb){
			gm('tugas').find().sort({mulai: 1}).exec().then(function(data){
				async.forEachSeries(data, function(item, cbb){
					var a = new Date(item.mulai);
					var b = new Date(item.selesai);
					var ddt = {
						_id : item._id,
						jenis : item.jenis,
						judul : item.judul,
						rincian : item.rincian,
						matakuliahRef : item.matakuliahRef,
						mulai : item.mulai==null ? '':moment(a).format('DD-MM-YYYY'),
						selesai : item.selesai==null ? '':moment(b).format('DD-MM-YYYY'),
						matakuliah : '',
						dosen : ''
					};
					if(item.matakuliahRef){
						gm('matakuliah').findOne({_id:item.matakuliahRef}).exec().then(function(mk){
							gm('profile').findOne({_id:mk.dosenRef}).exec().then(function(pr){
								ddt.matakuliah = mk ? mk.matakuliah:'';
								ddt.gambar = mk ? mk.gambar:'';
								ddt.dosen = pr ? pr.name:'';
								daftar.push(ddt);
								cbb();
							}).catch(function(err){
								cbb(err);
							});
						}).catch(function(err){
							cbb();
						});
					}else{
						daftar.push(ddt);
						cbb();
					}
				}, function(err){
					if(err)
						cb(err);
					cb();
				});
			}).catch(function(err){
				cb(err);
			});
		}
	], function(err){
		if(err)
			res.send({status:false, data: []});
		else
			res.send({status:true, data: daftar});
	});
});

router.get('/data/kegiatan', function(req, res, next){
	daftar = new Array;

	async.parallel([
			function(cb){
				gm('kegiatan').find().sort({pelaksanaan: 1}).exec().then(function(data){
					async.forEachSeries(data, function(item, cbb){
						gm('profile').findOne({_id:item.kontakRef}).exec().then(function(kontak){
							var a = new Date(item.pelaksanaan);
							var b = new Date(item.sampai);
							var fdata = {
								_id: item._id,
								nama: item.nama,
								deskripsi: item.deskripsi,
								pelaksanaan: item.pelaksanaan==null ? '':moment(a).format('DD-MM-YYYY'),
								sampai: item.sampai==null ? '':moment(b).format('DD-MM-YYYY'),
								kontakRef: item.kontakRef,
								gambar: item.gambar,
								kontak: ''
							};
							if(kontak)
								fdata.kontak = kontak.name;
							daftar.push(fdata);
							cbb();
						}).catch(function(err){
							console.log(err);
						});
					}, function(err){
						if(err)
							console.log(err);
						cb();
					});
				}).catch(function(err){
					console.log(err);
				});
			}
		], function(err){
			if(err)
				res.send({status:false, data: []});
			else
				res.send({status:true, data: daftar});
		});
});

router.get('/data/timeline', function(req, res, next){
	daftar = new Array;

	gm('timeline').find().sort({awal: 1}).exec().then(function(data){
		async.forEachSeries(data, function(item, cbb){
			var a = new Date(item.awal);
			var b = new Date(item.akhir);
			daftar.push({
				_id: item._id,
				timeline: item.timeline,
				detail: item.detail,
				awal: item.awal==null ? '':moment(a).format('DD-MM-YYYY'),
				akhir: item.akhir==null ? '':moment(b).format('DD-MM-YYYY'),
				divisi: item.divisi,
				gambar: item.gambar
			});
			cbb();
		}, function(err){
			if(err)
				res.send({status:false, data: []});
			else
				res.send({status:true, data: daftar});
		});
	}).catch(function(err){
		res.send({status: false});
	});
});

router.get('/data/informasi', function(req, res, next){
	daftar = new Array;
	entities = new Entities();

	gm('informasi').find().sort({bulan: 1}).exec().then(function(data){
		async.forEachSeries(data, function(item, cbb){
			dt = item.bulan;
			bulan = tahun = '';
			if(dt!=null && dt!=undefined){
	            tahun = dt.getFullYear();
	            bulan = dt.getMonth();
	        }
			daftar.push({
				_id: item._id,
				judul: item.judul,
				detail: item.detail,
				html_detail: entities.decode(item.detail),
				bulan: params.bulan[bulan],
				tahun: tahun,
				gambar: item.gambar
			});
			cbb();
		}, function(err){
			if(err)
				res.send({status:false, data: []});
			else
				res.send({status:true, data: daftar});
		});
	}).catch(function(err){
		res.send({status:false, data: []});
	})
});

router.get('/data/profile/:profileRef', function(req, res, next) {
	pref = req.params.profileRef;
	gm('profile').findOne({_id:pref}).exec().then(function(rs){
		var a = new Date(rs.date_birth);
		var b = new Date(rs.date_in);
		var c = new Date(rs.date_out);
		date_birth = moment(a).format('DD-MM-YYYY');
		date_in = moment(b).format('DD-MM-YYYY');
		date_out = moment(c).format('DD-MM-YYYY');
		profil = {
			'_id': rs._id,
			'name': rs.name,
			'last_login': rs.last_login,
			'photo': rs.photo,
			'date_birth': date_birth,
			'date_out': date_out,
			'date_in': date_in,
			'level': rs.level,
			'position': rs.position,
			'phone': rs.phone,
			'email': rs.email,
			'address': rs.address,
			'place_birth': rs.place_birth,
			'gender': rs.gender,
			'ni': rs.ni
		};
		res.send({status: true, data: profil});
	}).catch(function(err){
		console.log(err);
		res.send({status: false});
	});
});

router.get('/angkatan', function(req, res, next){
	gm('profile').find({position:'anggota'}).sort({date_in:1}).exec().then(function(mh){
		var tahun = new Array;
		for(imh in mh){
			var din = mh[imh].date_in;
			if(din!=null){
				if(!_.contains(tahun, din.getFullYear()) && (din.getFullYear()>2010))
					tahun.push(din.getFullYear());
			}
		}
		console.log('angkatan', tahun);
		res.send({status:true, angkatan:tahun});
	}).catch(function(err){
		console.log(err);
		res.send({status:false, angkatan:[]});
	});
});

module.exports = router;