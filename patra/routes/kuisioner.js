var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var Entities = require('html-entities').AllHtmlEntities;
var multer = require('multer');

var Profile = require('../models/profile');
var Kuisioner = require('../models/kuisioner');
var KuisionerDetail = require('../models/kuisioner_detail');
var KuisionerUser = require('../models/kuisioner_user');

var params = {
	module: 'kuisioner',
	data: []
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

var storage = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/kuis');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname);
  	}
});

var upload = multer({storage: storage});

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

router.get('/', function(req, res, next) {
	params.data = new Array;
	entities = new Entities();
	Kuisioner.find().exec().then(function(data){
		async.forEach(data, function(item, cbb){
			var a = new Date(item.created_at);
			var btncol = 'default';
			if(item.status=='close')
				btncol = 'btn-warning';
			if(item.status=='open')
				btncol = 'btn-success';
			if(item.status=='lock')
				btncol = 'btn-danger';
			params.data.push({
				_id: item._id,
				judul: item.judul,
				deskripsi: item.deskripsi,
				status: item.status,
				gambar: item.gambar,
				html_deskripsi: item.deskripsi==undefined?'':entities.decode(item.deskripsi),
				tanggal: moment(a).format('DD-MM-YYYY'),
				btncol: btncol
			});
			cbb();
		}, function(err){
			if(err)
				next(err);
			params.random = random(1, 100);
			res.render('views/kuisioner', { title: 'Data Kuisioner', data: params });
		});
	}).catch(function(err){
		next(err);
	});
});

router.post('/save', upload.single('gambar'), function(req, res, next){
	var form_data = {
		judul: req.body.judul,
		deskripsi: req.body.deskripsi,
		gambar: req.file==undefined?'':req.file.filename,
		created_at: new Date
	};
	var kuisioner = new Kuisioner(form_data);
	kuisioner.save().then(function(data){
		res.redirect('/' + params.module);
	}).catch(function(err){
		console.log(err);
		next(err);
	});
});

router.post('/update', upload.single('gambar'), function(req, res, next){
	var id = req.body.id;
	Kuisioner.findOne({_id: id}).exec().then(function(keg){
		if(keg){
			keg.judul = req.body.judul;
			keg.deskripsi = req.body.deskripsi;
			keg.updated_at = new Date;
			if(req.file!=undefined)
				keg.gambar = req.file.filename;
			keg.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.get('/detail/:kid', function(req, res, next){
	var kid = req.params.kid;
	KuisionerDetail.find({kuisionerRef:kid}).exec().then(function(kuis){
		params.data = kuis;
		params.kuisionerRef = kid;
		res.render('views/kuisioner_detail', { title: 'Data Kuisioner Detail', data: params });
	}).catch(function(err){
		params.data = [];
		res.render('views/kuisioner_detail', { title: 'Data Kuisioner Detail', data: params });
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		KuisionerDetail.findOne({kuisionerRef: id}).exec().then(function(keg){
			if(!keg)
				Kuisioner.find({_id:id}).remove().exec();
			res.send({status: true, message: 'Data deleted'});
		}).catch(function(err){
			res.send({status: false});
		});
	}else
		res.send({status: false});
});

router.post('/save_detail', function(req, res, next){
	var form_data = {
		kuisionerRef: req.body.kuisionerRef,
		pertanyaan: req.body.pertanyaan,
		tipe: req.body.tipe,
		choice: new Array
	};
	async.forEach(req.body.choice, function(item, cbb){
		form_data.choice.push({label: item, value: item});
		cbb();
	}, function(err){
		if(err)
			next(err);
		if(form_data.tipe=='text' && form_data.choice!=undefined)
			delete form_data.choice;
		var kuisioner_detail = new KuisionerDetail(form_data);
		kuisioner_detail.save().then(function(data){
			res.redirect('/' + params.module);
		}).catch(function(err){
			next(err);
		});
	});
});

router.post('/update_detail', function(req, res, next){
	var id = req.body.id;
	KuisionerDetail.findOne({_id: id}).exec().then(function(keg){
		if(keg){
			keg.pertanyaan = req.body.pertanyaan;
			keg.tipe = req.body.tipe;
			if(keg.tipe=='text' && keg.choice!=undefined)
				delete keg.choice;
			keg.choice = new Array;
			async.forEach(req.body.choice, function(item, cbb){
				if(Array.isArray(req.body.choice))
					keg.choice.push({label: item, value: item});
				cbb();
			}, function(err){
				if(err)
					next(err);
				if(!Array.isArray(req.body.choice))
					keg.choice.push({label: req.body.choice, value: req.body.choice});
				keg.save().then(function(data){
					res.redirect('/' + params.module);
				}).catch(function(err){
					next(err);
				});
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/disable', function(req, res, next){
	var id = req.body.id;
	var status = req.body.status;
	if(id!=undefined){
		Kuisioner.findOne({_id:id}).exec().then(function(kuis){
			kuis.status = status;
			kuis.save().then(function(ks){
				res.send({status: true, message: 'Status updated'});
			}).catch(function(er){
				res.send({status: false});
			});
		}).catch(function(err){
			res.send({status: false});
		});
	}else
		res.send({status: false});
});

router.post('/delete_detail', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		KuisionerDetail.find({_id:id}).remove().exec();
		res.send({status: true, message: 'Data deleted'});
	}else
		res.send({status: false});
});

router.post('/duplicate_detail', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		KuisionerDetail.findOne({_id:id}).exec().then(function(dd){
			var fd = {
				kuisionerRef: dd.kuisionerRef,
				pertanyaan: dd.pertanyaan,
				tipe: dd.tipe
			}
			if(dd.choice!=undefined)
				fd.choice = dd.choice;
			var sd = new KuisionerDetail(fd);
			sd.save().then(function(data){
				res.send({status: true, message: 'Data duplicated'});
			}).catch(function(err){
				next(err);
			});
		}).catch(function(err){
			res.send({status: false});
		});
	}else
		res.send({status: false});
});

router.post('/status', function(req, res, next){
	var id = req.body.id;
	var newstatus = req.body.status;
	if(id!=undefined){
		Kuisioner.findOne({_id: id}).exec().then(function(kuis){
			if(kuis){
				kuis.status = newstatus;
				kuis.save().then(function(data){
					res.redirect('/' + params.module);
				}).catch(function(err){
					res.redirect('/' + params.module);
				});
			}else
				res.redirect('/' + params.module);
		}).catch(function(err){
			res.redirect('/' + params.module);
		});
	}else
		res.redirect('/' + params.module);
});

module.exports = router;
