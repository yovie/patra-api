var express = require('express');
var async = require('async');
var _ = require('underscore');
var router = express.Router();
var moment = require('moment');
const mongoose = require('mongoose');

var Jadwal = require('../models/jadwal');
var Matakuliah = require('../models/matakuliah');
var Profile = require('../models/profile');

var params = {
	module: 'jadwal-kuliah',
	hari: ['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu']
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function getDataAngkatan(cb){
	Profile.find({position:'anggota'}).sort({date_in:1}).exec().then(function(mh){
		var tahun = new Array;
		for(imh in mh){
			var din = mh[imh].date_in;
			if(din!=null){
				if(!_.contains(tahun, din.getFullYear()) && (din.getFullYear()>2010))
					tahun.push(din.getFullYear());
			}
		}
		cb(tahun);
	}).catch(function(err){
		cb(false);
	});
}

router.all('/', function(req, res, next) {
	tmps = new Array;
	mk1 = new Array;
	daftar = new Array;
	params.data = new Array;
	params.max_hari = 0;
	params.matakuliah = new Array;
	params.angkatan = new Array;
	params.default_angkatan = 0;

	async.waterfall([
		function(cb){
			getDataAngkatan((hasil)=>{
				params.angkatan = hasil;
				if(Array.isArray(hasil))
					params.default_angkatan = Math.max.apply(null, hasil);
				cb();
			});
		},
		function(cb){
			if(req.query.angkatan!=undefined)
				params.default_angkatan = req.query.angkatan;
			if(req.body.filterangkatan!=undefined)
				params.default_angkatan = req.body.filterangkatan;
			Jadwal.find({angkatan: params.default_angkatan}).sort({jammulai:1}).exec().then(function(data){
				async.forEachSeries(data, function(item, cbb){
					Matakuliah.findOne({_id:item.matakuliahRef}).exec().then(function(matkul){
						// var jmulai = moment(item.jam_mulai, ["h:mm A"]).format("HH:mm");
						// var jselesai = moment(item.jam_selesai, ["h:mm A"]).format("HH:mm");
						tmps.push({
							_id: item._id,
							hari: item.hari,
							jam_mulai: item.jam_mulai,
							jam_selesai: item.jam_selesai,
							jammulai: item.jammulai,
							angkatan: item.angkatan,
							matakuliahRef: item.matakuliahRef,
							matakuliah: matkul ? matkul.matakuliah:'',
							matakuliah_kode: matkul ? matkul.kode:'',
							dosenRef: matkul ? matkul.dosenRef:''
						});
						cbb();
					}).catch(function(err){
						cbb(err);
					});
				}, function(err){
					// if(err)
						// cb(err);
					cb();
					// console.log(tmps);
				});
			}).catch(function(err){
				next(err);
			});
		},
		function(cb){
			// console.log('===0===',tmps);
			async.forEachSeries(tmps, function(item, cbb){
				if(mongoose.Types.ObjectId.isValid(item.dosenRef)){
					Profile.findOne({_id:item.dosenRef}).exec().then(function(dosen){
						if(dosen){
							item.nama_dosen = dosen?dosen.name:'';
							item.ni = dosen?dosen.ni:'';
						}
						daftar.push(item);
						cbb();
					}).catch(function(err){
						console.log(err);
						cbb(err);
					});
				}else
					cbb();
			}, function(err){
				// if(err)
				console.log(err);
				cb();
			});
		},
		function(cb){
			// console.log('===1===',daftar);
			Matakuliah.find().exec().then(function(matkul){
				mk1 = matkul?matkul:{};
				cb();
			}).catch(function(err){
				next(err);
			});
		},
		function(cb){
			async.forEach(mk1, function(item, cbb){
				Profile.findOne({_id:item.dosenRef}).exec().then(function(dosen){
					params.matakuliah.push({
						_id: item._id,
						kode: item.kode,
						matakuliah: item.matakuliah,
						dosen: dosen?dosen.name:''
					});
					cbb();
				}).catch(function(err){
					cbb(err);
				});
			}, function(err){
				if(err)
					cb(err);
				cb();
			});
		},
		function(cb){
			// console.log('==========daftar=====', daftar);
			async.forEachSeries(daftar, function(item, cbb){
				if(!Array.isArray(params.data[item.hari]))
					params.data[item.hari] = new Array;
				params.data[item.hari].push(item);
				if(params.max_hari<params.data[item.hari].length)
					params.max_hari = params.data[item.hari].length;
				cbb();
			}, function(err){
				if(err)
					cb(err);
				cb();
			});	
		}
	], function(err){
		if(err)
			next(err);
		params.random = random(1, 100);
		res.render('views/jadwal', { title: 'Jadwal Kuliah', data: params });
	});
});

router.post('/save', function(req, res, next){
	var tt = 0;
	if(req.body.jam_mulai!=undefined){
		tt = moment(req.body.jam_mulai, 'HH:mm').unix();
	}
	var form_data = {
		hari: req.body.hari,
		jam_mulai: req.body.jam_mulai,
		jam_selesai: req.body.jam_selesai,
		matakuliahRef: req.body.matakuliahRef=='' ? null:req.body.matakuliahRef,
		angkatan: req.body.angkatan,
		jammulai: tt
	};
	var jadwal = new Jadwal(form_data);
	jadwal.save().then(function(data){
		res.redirect('/' + params.module + '?angkatan=' + req.body.angkatan);
	}).catch(function(err){
		next(err);
	});
});

router.post('/update', function(req, res, next){
	var id = req.body.id;
	Jadwal.findOne({_id: id}).exec().then(function(jadwal){
		if(jadwal){
			var tt = 0;
			if(req.body.jam_mulai!=undefined){
				tt = moment(req.body.jam_mulai, 'HH:mm').unix();
			}
			jadwal.hari = req.body.hari;
			jadwal.jam_mulai = req.body.jam_mulai;
			jadwal.jam_selesai = req.body.jam_selesai;
			jadwal.matakuliahRef = req.body.matakuliahRef=='' ? null:req.body.matakuliahRef;
			jadwal.angkatan = req.body.angkatan;
			jadwal.jammulai = tt;
			jadwal.save().then(function(data){
				res.redirect('/' + params.module + '?angkatan=' + req.body.angkatan);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module + '?angkatan=' + req.body.angkatan);
	}).catch(function(err){
		next(err);
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		Jadwal.find({_id:id}).remove().exec();
	}
	res.send({status: true, message: 'Data deleted'});
});

module.exports = router;
