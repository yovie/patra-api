var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var Entities = require('html-entities').AllHtmlEntities;
var multer = require('multer');

var Timeline = require('../models/timeline');

var params = {
	module: 'karya'
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

var storage = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/tmline');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname);
  	}
});

var upload = multer({storage: storage});

router.get('/', function(req, res, next) {
	params.data = new Array;
	params.random = random(1, 100);
	entities = new Entities();
	Timeline.find().exec().then(function(data){
		async.forEach(data, function(item, cbb){
			// var a = new Date(item.awal);
			// var b = new Date(item.akhir);
			params.data.push({
				_id: item._id,
				timeline: item.timeline,
				detail: item.detail,
				html_detail: entities.decode(item.detail),
				// awal: item.awal==null ? '':moment(a).format('DD-MM-YYYY'),
				// akhir: item.akhir==null ? '':moment(b).format('DD-MM-YYYY'),
				divisi: item.divisi,
				gambar: item.gambar
			});
			cbb();
		}, function(err){
			if(err)
				cb(err);
			res.render('views/timeline', { title: 'Karya PATRA', data: params });
		});
	}).catch(function(err){
		next(err);
	})
});

router.post('/save', upload.single('gambar'), function(req, res, next){
	var form_data = {
		timeline: req.body.timeline,
		detail: req.body.detail,
		// awal: breakDate(req.body.awal),
		// akhir: breakDate(req.body.akhir),
		divisi: req.body.divisi,
		gambar: req.file==undefined?'':req.file.filename
	};
	var timeline = new Timeline(form_data);
	timeline.save().then(function(data){
		res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/update', upload.single('gambar'), function(req, res, next){
	var id = req.body.id;
	Timeline.findOne({_id: id}).exec().then(function(tm){
		if(tm){
			tm.timeline = req.body.timeline;
			tm.detail = req.body.detail;
			// tm.awal = breakDate(req.body.awal);
			// tm.akhir = breakDate(req.body.akhir);
			tm.divisi = req.body.divisi;
			if(req.file!=undefined)
				tm.gambar = req.file.filename;
			tm.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		Timeline.find({_id:id}).remove().exec();
	}
	res.send({status: true, message: 'Data deleted'});
});

module.exports = router;
