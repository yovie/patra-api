var express = require('express');
var router = express.Router();
var async = require('async');
var multer = require('multer');

var Matakuliah = require('../models/matakuliah');
var Profile = require('../models/profile');

var params = {
	module: 'matakuliah'
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

var storage = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/matkul');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname);
  	}
});

var upload = multer({storage: storage});

router.get('/', function(req, res, next) {
	params.data = new Array;
	async.parallel([
		function(cb){
			Matakuliah.find().exec().then(function(data){
				async.forEach(data, function(item, cbb){
					if(item.dosenRef){
						Profile.findOne({_id:item.dosenRef}).exec().then(function(dosen){
							params.data.push({
								_id: item._id,
								kode: item.kode,
								matakuliah: item.matakuliah,
								sks: item.sks,
								dosenRef: item.dosenRef,
								nama_dosen: (dosen!=null) ? dosen.name:'',
								gambar: item.gambar
							});
							cbb();
						}).catch(function(err){
							console.log(err);
						});
					}else{
						params.data.push({
							_id: item._id,
							kode: item.kode,
							matakuliah: item.matakuliah,
							sks: item.sks,
							dosenRef: item.dosenRef,
							nama_dosen: '',
							gambar: item.gambar
						});
						cbb();
					}
				}, function(err){
					if(err){
						console.log(err);
						cb(err);
					}else
						cb();
				});
			}).catch(function(err){
				console.log(err);
			});
		},
		function(cb){
			Profile.find({position: 'dosen'}).exec().then(function(data){
				params.dosen = data;
				cb();
			}).catch(function(err){
				console.log(err);
			});
		}
	], function(err){
		if(err){
			console.log(err);
			// next(err);
		}
		params.random = random(1, 100);
		res.render('views/matakuliah', { title: 'Data matakuliah', data: params });
	});
});

router.post('/save', upload.single('gambar'), function(req, res, next){
	var form_data = {
		kode: req.body.kode,
		matakuliah: req.body.matakuliah,
		sks: req.body.sks,
		dosenRef: req.body.dosenRef=='' ? null:req.body.dosenRef,
		gambar: req.file==undefined?'':req.file.filename
	};
	var matakul = new Matakuliah(form_data);
	matakul.save().then(function(data){
		res.redirect('/' + params.module);
	}).catch(function(err){
		console.log(err);
		next(err);
	});
});

router.post('/update', upload.single('gambar'), function(req, res, next){
	var id = req.body.id;
	Matakuliah.findOne({_id: id}).exec().then(function(matkul){
		if(matkul){
			matkul.kode= req.body.kode;
			matkul.matakuliah= req.body.matakuliah;
			matkul.sks= req.body.sks;
			matkul.dosenRef= req.body.dosenRef=='' ? null:req.body.dosenRef;
			if(req.file!=undefined)
				matkul.gambar = req.file.filename;
			matkul.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		console.log(err);
		next(err);
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		Matakuliah.find({_id:id}).remove().exec();
	}
	res.send({status: true, message: 'Data deleted'});
});

module.exports = router;
