var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var Entities = require('html-entities').AllHtmlEntities;
var multer = require('multer');
var mongoose = require('mongoose');

var Profile = require('../models/profile');
var Kuisioner = require('../models/kuisioner');
var KuisionerDetail = require('../models/kuisioner_detail');
var KuisionerUser = require('../models/kuisioner_user');

var params = {
	module: 'kuisioner-result',
	data: []
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

router.get('/', function(req, res, next) {
	var ids = req.query.ids;
	var kuisioner;
	var user = new Array;
	var kuisionerDetail;
	var kuisionerUser = new Array;
	async.waterfall([
		function(cb){
			Kuisioner.findOne({_id:ids}).exec().then(kuis=>{
				kuisioner = kuis;
				cb();
			});
		},
		function(cb){
			KuisionerDetail.find({kuisionerRef:kuisioner._id}).exec().then(lkuis=>{
				kuisionerDetail = lkuis;
				cb();
			});
		},
		function(cb){
			async.forEachSeries(kuisionerDetail, function(itm, cbb){
				KuisionerUser.find({kuisionerDetailRef:itm._id}).exec().then(ku=>{
					for(var ii=0; ii<ku.length; ii++){
						if(kuisionerUser.indexOf(ku[ii].kontakRef.toString())<0)
							kuisionerUser.push(ku[ii].kontakRef.toString());
					}
					cbb();
				});
			}, function(err){
				// console.log(kuisionerUser);
				cb();
			});
		},
		function(cb){
			async.forEachSeries(kuisionerUser, function(itm, cbb){
				Profile.findOne({userRef:itm}).exec().then(users=>{
					user.push(users);
					cbb();
				});
			}, function(err){
				console.log(user);
				cb();
			});
		}
	], function(err){
		params.random = random(1, 100);
		res.render('views/kuisioner-result', { title: 'Data Kuisioner', data: params, kuisioner: kuisioner, users: user });
	});
});

router.get('/detail', function(req, res, next) {
	var kuisid = mongoose.Types.ObjectId(req.query.kuisid);
	var userid = mongoose.Types.ObjectId(req.query.userid);
	var kuisionerDetail;
	var jawaban = new Array;
	async.waterfall([
		function(cb){
			KuisionerDetail.find({kuisionerRef:kuisid}).exec().then(lkuis=>{
				kuisionerDetail = lkuis;
				cb();
			});
		},
		function(cb){
			async.forEachSeries(kuisionerDetail, function(itm, cbb){
				console.log(itm._id, userid);
				KuisionerUser.findOne({kuisionerDetailRef: itm._id, kontakRef: userid}).exec().then(ku=>{
					console.log('kuisionerUser', ku);
					jawaban.push({
						pertanyaan: itm.pertanyaan,
						tipe: itm.tipe,
						choice: itm.choice,
						choiceValue: (ku==null) ? '':ku.choiceValue,
						textValue: (ku==null) ? '':ku.textValue
					});
					cbb();
				});
			}, function(err){
				cb();
			});
		}
	], function(err){
		console.log(jawaban);
		res.render('views/kuisioner-jawaban', { jawaban: jawaban, kuisid: kuisid, module: params.module });
	});
});

module.exports = router;
