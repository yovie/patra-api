var express = require('express');
var router = express.Router();
var async = require('async');

var Profile = require('../models/profile');


var params = {};

router.get('/', function(req, res, next) {
	async.waterfall([
		function(cb){
			Profile.count({}, function(err, count){
				if(err)
					params.user_count = 0;
				else
					params.user_count = count;
				cb();
			});
		}
	], function(err){
		if(err)
			next(err);
		res.render('index', { title: 'Dashboard', data: params });
	});
});

module.exports = router;
