var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
	res.render('views/settings', { title: 'Settings' });
});

module.exports = router;
