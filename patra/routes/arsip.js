var express = require('express');
var router = express.Router();
var async = require('async');
var moment = require('moment');
var fs = require('fs');
var multer = require('multer');
var Entities = require('html-entities').AllHtmlEntities;

var Arsip = require('../models/arsip');

var params = {
	module: 'arsip-data'
};

function random (low, high) {
    return Math.random() * (high - low) + low;
}

function breakDate(datestr){	//format dd-mm-yyyy
	if(datestr==undefined)
		return null;
	if(datestr.length!=10)
		return null;
	var fmt = {
		day: datestr.substr(0,2),
		month: datestr.substr(3,2),
		year: datestr.substr(6,4)
	};
	var dt = new Date(parseInt(fmt.year), parseInt(fmt.month)-1, parseInt(fmt.day), 0, 0, 0, 0);
	return dt;
}

function createDate(bln, thn){
	// console.log(thn, bln);
	if(bln==undefined||thn==undefined)
		return null;
	var dt = new Date(parseInt(thn), parseInt(bln)-1, 1, 0, 0, 0, 0);
	// console.log('datena', dt);
	return dt;	
}

var storage = multer.diskStorage({
	destination: function (request, file, callback) {
    	callback(null, __dirname + '/../files/arsip');
  	},
  	filename: function (request, file, callback) {
  		var d = new Date();
		var n = d.getTime();
    	callback(null, n +'-'+ file.originalname)
  	}
});

var upload = multer({storage: storage});

var cpUpload = upload.fields([{ name: 'berkas', maxCount: 1 }, { name: 'gambar', maxCount: 1 }]);

router.get('/', function(req, res, next) {
	params.data = new Array;
	params.random = random(1, 100);
	entities = new Entities();
	Arsip.find().exec().then(function(data){
		async.forEach(data, function(item, cbb){
			params.data.push({
				_id: item._id,
				nama: item.nama,
				keterangan: item.keterangan,
				html_keterangan: entities.decode(item.keterangan),
				berkas: item.berkas,
				gambar: item.gambar
			});
			cbb();
		}, function(err){
			if(err)
				cb(err);
			res.render('views/arsip', { title: 'Arsip Data', data: params });
		});
	}).catch(function(err){
		next(err);
	})
});

router.post('/save', cpUpload, function(req, res, next){
	var form_data = {
		nama: req.body.nama,
		keterangan: req.body.keterangan,
		berkas: '',
		gambar: ''
	};
	if(req.files.berkas!=undefined && req.files.berkas[0]!=undefined)
		form_data.berkas = req.files.berkas[0].filename; 
	if(req.files.gambar!=undefined &&req.files.gambar[0]!=undefined)
		form_data.gambar = req.files.gambar[0].filename; 
	var arsip = new Arsip(form_data);
	arsip.save().then(function(data){
		res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

// upload.single('berkas')

router.post('/update', cpUpload, function(req, res, next){
	var id = req.body.id;
	Arsip.findOne({_id: id}).exec().then(function(tm){
		if(tm){
			tm.nama = req.body.nama;
			tm.keterangan = req.body.keterangan;
			if(req.files.berkas!=undefined && req.files.berkas[0]!=undefined)
				tm.berkas = req.files.berkas[0].filename; 
			if(req.files.gambar!=undefined && req.files.gambar[0]!=undefined)
				tm.gambar = req.files.gambar[0].filename; 
			tm.save().then(function(data){
				res.redirect('/' + params.module);
			}).catch(function(err){
				next(err);
			});
		}else
			res.redirect('/' + params.module);
	}).catch(function(err){
		next(err);
	});
});

router.post('/delete', function(req, res, next){
	var id = req.body.id;
	if(id!=undefined){
		Arsip.find({_id:id}).remove().exec();
	}
	res.send({status: true, message: 'Data deleted'});
});

module.exports = router;
